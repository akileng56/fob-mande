package com.example.mande;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
 
import org.json.JSONException;
 
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.mande.activity.AppSettings;
import com.mande.activity.Login;
import com.mande.activity.MainMenuActivity;
import com.mande.dialog.AppOpenDialog;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class MyProjects extends ActionBarActivity {
ProgressDialog pDialog;
ListView listView;
ImageView menubtn;
 
public static final String SELECTED_PROJECT = "MyPrefs" ; 

public static final String Project_Has_Template = "MyPreference" ;
 
String Projectname, s="";
ArrayList<String> data = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_my_projects);
		listView = (ListView)findViewById(R.id.listView1);
		new LoginCheckAsyncTask().execute();
		
		
		menubtn = ((ImageView) findViewById(R.id.exit1));
		
		menubtn.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		        PopupMenu popup = new PopupMenu(MyProjects.this, menubtn);
		        //Inflating the Popup using xml file
		        popup.getMenuInflater()
		                .inflate(R.menu.popup_menu, popup.getMenu());

		        //registering popup with OnMenuItemClickListener
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {

		                switch (item.getItemId()){
		                    case R.id.help:
		                        /*getSupportFragmentManager().beginTransaction()
		                                .add(R.id.container, new SelfCareCategorySelectionFragment()).addToBackStack(null)
		                                .commit();*/
		                        break;
		                    case R.id.logout:
		                        Intent intent = new Intent(MyProjects.this, Login.class);
		                        startActivity(intent);
		                        finish();
		                        break;
		                    case R.id.settings:
		                        Intent i = new Intent(MyProjects.this, AppSettings.class);
		                        startActivity(i);
		                        break;

		                }

		                return true;
		            }
		        });

		        popup.show();
		    }
		});
	}

	private class LoginCheckAsyncTask extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MyProjects.this);
			pDialog.setMessage("Loading......");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
 
			// TODO Auto-generated method stub 
			 
 
				SharedPreferences settings = getSharedPreferences(Login.LOGGED_IN_USER,0);
				      
				 String person = settings.getString("FullName", null); 
				 
 
				HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
						+"ActiveProjects?" );			
 
	
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("FullName",person)); 
			 
			
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {
 
			final JSONArray array;
 

			if (result != null)  
				 Toast.makeText(getBaseContext(),s+" Saved successfully!",
						 Toast.LENGTH_SHORT).show();  
				try{
                data.clear();
 
                array = new JSONArray(result);
 

                for(int a=0;a<array.length();a++){
                	    
	                    	 JSONObject p = array.getJSONObject(a);
	                      

	                    	String projectname = p.getString("Name");
 
	                    	String CheckTemplate = p.getString("HasTemplate");
	                            data.add( projectname ); 
	                           
 
	                            
	                            
                }
                
                if(data.size() > 0)
                {
                 ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyProjects.this, 
                            android.R.layout.simple_list_item_1, data);

                          listView.setAdapter(adapter);
                    
                      listView.setOnItemClickListener(new OnItemClickListener() {
                          

							@SuppressLint("NewApi")
							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, final int position, long id) {
								// TODO Auto-generated method stub
 
								Projectname = (String) parent.getItemAtPosition(position);							 
 
								 
									SharedPreferences settings = getSharedPreferences(SELECTED_PROJECT ,0);
								    SharedPreferences.Editor editor = settings.edit();
								    editor.putString("Project",Projectname);
								    editor.commit();
								 
 
								     for(int j=0;j<array.length();j++){
					                	    
				                    	 JSONObject u;
										try {
											u = array.getJSONObject(j);
											
											String projectName = u.getString("Name");
					                    	String CheckTemplate = u.getString("HasTemplate");
					                    	
					                             if(projectName == Projectname){
					                            	 
					                            		SharedPreferences Projectsetting = getSharedPreferences(Project_Has_Template ,0);
					        						    SharedPreferences.Editor Project_editor = Projectsetting.edit();
					        						    Project_editor.putString("HasTemplate",CheckTemplate);
					        						    Project_editor.commit(); 
					                             }
											
											
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
											}	                           			                           
							                       
						                }
 
								 
							   startActivity(new Intent(MyProjects.this,MainMenuActivity.class));								
				 		}
                      });
            		
                    
                }
                
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }        
			
			pDialog.dismiss();
	     
		} 
	

	}  
	
	
	@Override
	public void onBackPressed() {
		final AppOpenDialog dialog = new AppOpenDialog(this,
				"  INFORMATION  ", " WOULD YOU LIKE TO EXIT MANDE APP??    ");
		dialog.show();
		dialog.setOkClickedAction(new OnClickListener() {
			@Override
			public void onClick(View v) { 
				dialog.dismiss();	
			 
			}
		});
		
		dialog.setyesClickedAction(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();			
				
			startActivity(new Intent(MyProjects.this,Login.class));
 
			}
		});
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.stories, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
			//startActivity(new Intent(Stories.this,NewStory.class));
		}
		return super.onOptionsItemSelected(item);
	}
}
