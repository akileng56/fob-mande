package com.example.mande;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
 
import java.io.UnsupportedEncodingException;
 
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
 
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
 
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
 
import android.content.SharedPreferences;
 
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
 
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mande.activity.Stories;
 
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

@SuppressLint("NewApi")
public class AttachDocument extends ListActivity{
 
	  private String path;
	  String Title, strFile="", send;
	  Bundle bundle;
	  String filename;
 
	  TextView Forpath;
 
	  URL Docfile;
      ProgressDialog pDialog;
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
 
	    requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    setContentView(R.layout.activity_attach_document);
    
	    bundle = getIntent().getExtras();
	    Title = bundle.getString("title");
	    Forpath = (TextView)findViewById(R.id.textpath);
 
	    
	    // Use the current directory as title
	    path = "/storage";
	    if (getIntent().hasExtra("path")) {
	      path = getIntent().getStringExtra("path");
	    }
 
	    //setTitle(path);
	    Forpath.setText(path);
 

	    // Read all files sorted into the values-array
	    List values = new ArrayList();
	    File dir = new File(path);
	    if (!dir.canRead()) {
 
	    	new AlertDialog.Builder(this)
            .setTitle("Not Accessible")                
            .setNeutralButton("OK", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int button){
                	path = "/storage";
                }
            })
            .show();
	    	
	      //setTitle(getTitle() + " (inaccessible)");
	     //Forpath.setText(getTitle() + "(inaccessible)");
 
	    }
	    String[] list = dir.list();
	    if (list != null) {
	      for (String file : list) {
	        if (!file.startsWith(".")) {
	          values.add(file);
	        }
	      }
	    }
	    Collections.sort(values);

	    // Put the data into the list
	    ArrayAdapter adapter = new ArrayAdapter(this,
	        android.R.layout.simple_list_item_2, android.R.id.text1, values);
	    setListAdapter(adapter);
	  }

	  
	  
	  
	  @Override
	  protected void onListItemClick(ListView l, View v, int position, long id) {
	     filename = (String) getListAdapter().getItem(position);
	    if (path.endsWith(File.separator)) {
	      filename = path + filename;
	    } else {
	      filename = path + File.separator + filename;
	    }
	    if (new File(filename).isDirectory()) {
	      Intent intent = new Intent(this, AttachDocument.class);
	      intent.putExtra("path", filename);
	      startActivity(intent);
	    } else {
			//new File(filename);
 
			if((filename.endsWith("mp3")) || filename.endsWith("mp4") || filename.endsWith("doc") || filename.endsWith("pdf") ){	
				 
				  
			/*	Intent gotoupload = new Intent(AttachDocument.this,UploadToServer.class);
				gotoupload.putExtra("title",Title);
				gotoupload.putExtra("filename",filename);
				startActivity(gotoupload);*/
				new SaveNewStory().execute();	
		
			  
 
			}	    
			else {
				new AlertDialog.Builder(this)
                .setTitle("Format is Not Allowed")                
                .setNeutralButton("OK", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int button){
                        //do nothing
                    }
                })
                .show();			   
			}
		}
	  }
	
	
	  
		private class SaveNewStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
			protected void onPreExecute() {
				super.onPreExecute();			
				pDialog = new ProgressDialog(AttachDocument.this);
				pDialog.setMessage("Loading....");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(true);
				pDialog.show();
				
			}
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				 
 
				 SharedPreferences settings = getSharedPreferences(Stories.SELECTED_STORY,0);
			      
				 String h = settings.getString("Story", null);
				 
				HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
						+"GetDoc?FullName="+h);	
				
			 
				File file = new File(filename);
			 
				 
 
				
				InputStreamEntity reqEntity;
				try {
					reqEntity = new InputStreamEntity(new FileInputStream(file), -1);
 
					 reqEntity.setContentType("application/octet-stream");
					    reqEntity.setChunked(true); // Send in multiple parts if needed
					    httpPost.setEntity(reqEntity);
					  
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   
				
				
				
				Log.e("url check", ""+httpPost);
				new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
				return null;
			} 
			
	 
			
			public void onCallBack(String result) {
				// TODO Auto-generated method stub
				 
				String resultCode, resultMsg;

				if (result != null) {
				
					JSONObject o;
					try { 
						o = new JSONObject(result);

						if (o.length() > 0) { 
							
							resultCode = o.getString("ResultCode");
							 resultMsg = o.getString("ResultMessage");		 
					 

							if (resultCode.equals("200")) {						
								 						
								 Toast.makeText(getBaseContext(),resultMsg,Toast.LENGTH_SHORT).show();
								 
								 
							} else if (resultCode.equals("201")) {
								
							 
								
							} 
						} 
					} catch (JSONException e) {
						e.printStackTrace(); 
		              
					}

				} else { 
					 
				} 
				 
				pDialog.dismiss(); 
			} 	
	      
		} 	
		
	 
	 

}
