package com.example.mande;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Getdoc extends ListActivity {
	public String path, filename="";
	public String init(){
		
	    setContentView(R.layout.activity_attach_document);

	    // Use the current directory as title
	    path = "/storage";
	    if (getIntent().hasExtra("path")) {
	      path = getIntent().getStringExtra("path");
	    }
	    setTitle(path);

	    // Read all files sorted into the values-array
	    List values = new ArrayList();
	    File dir = new File(path);
	    if (!dir.canRead()) {
	      setTitle(getTitle() + " (inaccessible)");
	    }
	    String[] list = dir.list();
	    if (list != null) {
	      for (String file : list) {
	        if (!file.startsWith(".")) {
	          values.add(file);
	        }
	      }
	    }
	    Collections.sort(values);

	    // Put the data into the list
	    ArrayAdapter adapter = new ArrayAdapter(this,
	        android.R.layout.simple_list_item_2, android.R.id.text1, values);
	    setListAdapter(adapter);
		
		return filename;
}

	  @Override
	  protected void onListItemClick(ListView l, View v, int position, long id) {
	    filename = (String) getListAdapter().getItem(position);
	    if (path.endsWith(File.separator)) {
	      filename = path + filename;
	    } else {
	      filename = path + File.separator + filename;
	    }
	    if (new File(filename).isDirectory()) {
	      Intent intent = new Intent(this, AttachDocument.class);
	      intent.putExtra("path", filename);
	      startActivity(intent);
	    } else {
			//new File(filename);
			if(filename.endsWith("pdf")){
				
			  
				Toast.makeText(this, filename + "  Almost there, Just one Step", Toast.LENGTH_LONG).show();
			}	    
			else {
				new AlertDialog.Builder(this)
              .setTitle("Format is Not Allowed")                
              .setNeutralButton("OK", new DialogInterface.OnClickListener(){
                  public void onClick(DialogInterface dialog, int button){
                      //do nothing
                  }
              })
              .show();			   
			}
		}
	  }
	
	
	
	
}
