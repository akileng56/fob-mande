package com.example.mande;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

 
import com.mande.activity.AppSettings;
import com.mande.activity.Login;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

import android.R.integer;
import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class QuestionsEntry extends ActionBarActivity {
	
	 ProgressDialog pDialog;
	 ArrayList<String> questiondata = new ArrayList<String>();
	 ArrayList<String> questiondata2 = new ArrayList<String>();
	 
	 int[] questiondata3 =  new int[50];
	 String[] questiondata4 = new String[50];
	 String[] answers = new String[50];
	 int a=0, QuestionCounter=0;
	 EditText getQuestionAnswer;
	 TextView getQuestiontxt, getQuestionNum;
	 ImageButton nextQuestion, PrevQuestion;
	 Button StartRecording,StopRecording,PauseRecording;
	 
	 private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
	 private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
	 private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	 private MediaRecorder recorder = null;
	 private int currentFormat = 0;
	 private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
	 private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_questions_entry);
		
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		getQuestionAnswer = (EditText) findViewById(R.id.editTextAnswer);
		getQuestiontxt  = (TextView) findViewById(R.id.txtquestionholder);
		getQuestionNum  = (TextView) findViewById(R.id.question_Num_holder);
		
		
		
		nextQuestion = (ImageButton) findViewById(R.id.form_forward_button);
		PrevQuestion = (ImageButton) findViewById(R.id.form_back_button);
		
		StartRecording = (Button) findViewById(R.id.form_startRecording_button);
		
		StopRecording = (Button) findViewById(R.id.form_stopRecording_button);
		
		PauseRecording = (Button) findViewById(R.id.form_PauseRecording_button);
		new GetQuestions().execute();
      
		
		
		nextQuestion.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		    	answers[a]= getQuestionAnswer.getText().toString();
		    	if(a==(QuestionCounter-1)){
		    		
		    	}else{
		    		a =  a + 1;	
		    	}		    		    
		    	getQuestionNum.setText("Question"+" "+ questiondata3[a]);	
            	getQuestiontxt.setText(questiondata4[a]);
            	getQuestionAnswer.setText(" "); 
		    }
		});
		
		PrevQuestion.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		    	answers[a]= getQuestionAnswer.getText().toString();
		    	if(a==0){		    		
		    	}else{
		    	a =  a - 1;
		    	}
		    	getQuestionNum.setText("Question"+" "+ questiondata3[a]);	
            	getQuestiontxt.setText(questiondata4[a]);
            	getQuestionAnswer.setText(answers[a]); 
		    }
		});
		
		
		StartRecording.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		    	StopRecording.setVisibility(1);
		    	PauseRecording.setVisibility(1);
		    	StartRecording.setVisibility(View.GONE);
		    	
		    	   recorder = new MediaRecorder();
		    	    recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		    	    recorder.setOutputFormat(output_formats[currentFormat]);
		    	    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		    	    recorder.setOutputFile(getFilename());		    	  
		    	   
		    	        try {
							recorder.prepare();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    	        recorder.start();
		    	      
		    }
		});
		
		
		StopRecording.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		    	StartRecording.setVisibility(1);
		    	StopRecording.setVisibility(View.GONE);
		    	PauseRecording.setVisibility(View.GONE);
		    	
		    	   if (null != recorder) {
		    	        recorder.stop();
		    	        recorder.reset();
		    	        recorder.release();
		    	        recorder = null;
		    	    }
		      
		    }
		});
		
		
		
		PauseRecording.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		    	StartRecording.setVisibility(1);
		    	StopRecording.setVisibility(1);
		    	PauseRecording.setVisibility(View.GONE);
		    	   if (null != recorder) {    	       
		    	        
		    	        recorder.release();
		    	        recorder = null;
		    	    }
		      
		    }
		});
		
	}
    
	
	
	private String getFilename() {
	    String filepath = Environment.getExternalStorageDirectory().getPath();
	    File file = new File(filepath, AUDIO_RECORDER_FOLDER);
	    if (!file.exists()) {
	        file.mkdirs();
	    }
	    return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);
	}
	private class GetQuestions extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(QuestionsEntry.this);
			pDialog.setMessage("Retreiving Questions....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"RetrieveQuestions?" );			
 
			 
			 SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);		      
			 String h = settings.getString("Project", null); 
		
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("ProjectName",h));				

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {		 
              String QuestionTxt, QuestionNumber;
			if (result != null) 
				
			{
                try
                {
                	questiondata.clear();
                	
                	//questiondata.add("** QUESTIONS** "); 
                    JSONArray array = new JSONArray(result);

                    for(int t=0;t<array.length();t++){
                    	    
   	                    	 JSONObject p = array.getJSONObject(t);  	                      

   	                    	QuestionTxt = p.getString("Question");
   	                    	QuestionNumber = p.getString("QuestionOrder");
   	                    	questiondata3[t] = Integer.parseInt(QuestionNumber);
   	                    	questiondata4[t] = QuestionTxt;                          
  	                            
  	                         QuestionCounter =    QuestionCounter + 1;
                    }
                    
                    if(questiondata3.length > 0)
                    {
                    	getQuestionNum.setText("Question"+" "+ questiondata3[0]);	
                    	getQuestiontxt.setText(questiondata4[0]);
           
                    }
                    else
                    {
                        Toast.makeText(QuestionsEntry.this, "No Questions Found", Toast.LENGTH_LONG).show();
                    }

                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
				else {				
				
				//Methods.showDialog(getSupportFragmentManager(), msg, title, ItecAlertDialog.DIALOG_OK_ACTION_NONE);
			} 				
			pDialog.dismiss();
		} 
	
}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.questions_entry, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
