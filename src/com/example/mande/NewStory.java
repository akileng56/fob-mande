package com.example.mande;

 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicNameValuePair;
 
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

 
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

import com.mande.activity.Stories;
 
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class NewStory extends ActionBarActivity {
	ArrayList<String> data = new ArrayList<String>();
	ArrayList<String> data1 = new ArrayList<String>();
 
	EditText getTitle, getSummary, getFullStory;
	RadioGroup radioGroup;
	RadioButton radioButton;
	Bundle bundle;
	RelativeLayout Rlayout;
	private static final int SELECT_DOC = 3;
    private static final int SELECT_PICTURE = 1;
    private static final int SELECT_VIDEO = 2;
	Spinner spindrop,spindrop1,spindrop2;
	String p, Attachname="",Attachtype="null",filename,selectedImagePath, selectedDocumentPath, selectedVideoPath;
 
	ImageView menubtn,Save;
	ProgressDialog pDialog,mProgressDialog;
 public static final int DIALOG_UPLOAD_PROGRESS = 0;
	 
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_new_story);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
 
	 
 
		radioGroup  = (RadioGroup)findViewById(R.id.radioGroup1);
		spindrop2 = (Spinner) findViewById(R.id.spinner1); //Domain
		spindrop = (Spinner) findViewById(R.id.spinner2); // narratedby	    
		spindrop1 = (Spinner) findViewById(R.id.spinner3); // collectedby
	    getTitle = (EditText) findViewById(R.id.editText1);
	    getSummary = (EditText) findViewById(R.id.editText2);
 
	    getFullStory = (EditText) findViewById(R.id.editfullStory);
	    Rlayout = (RelativeLayout)findViewById(R.id.Fullmiddle2);
		new GetDropDownvalues().execute();
		
		 
	    menubtn = ((ImageView) findViewById(R.id.Add));
		
		menubtn.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		        PopupMenu popup = new PopupMenu(NewStory.this, menubtn);
		        //Inflating the Popup using xml file
		        popup.getMenuInflater()
		                .inflate(R.menu.attachment1, popup.getMenu());

		        //registering popup with OnMenuItemClickListener
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {

		                switch (item.getItemId()){
		                    case R.id.help:
		                    	  Intent intent = new Intent();
		  	                      intent.setType("image/*");
		  	                      intent.setAction(Intent.ACTION_GET_CONTENT);
		  	                      startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
		                        break;
		                    case R.id.logout:
		                   	      Intent intent1 = new Intent();
		                	      intent1.setAction(Intent.ACTION_GET_CONTENT);
		                          intent1.setType("video/*");                
		                          startActivityForResult(Intent.createChooser(intent1,"Select Video"), SELECT_VIDEO);
		                        break;
		                    case R.id.settings:
		                      /*  Intent intent2 = new Intent(EditStory.this,AttachDocument.class);
		                   	    intent2.putExtra("title", getTitle.getText().toString());
		                        startActivity(intent2);*/ 
		                    	
		                     Intent intentdoc = new Intent(Intent.ACTION_GET_CONTENT);
		                        intentdoc.setType("file/*");
		                        startActivityForResult(Intent.createChooser(intentdoc,"Select File"), SELECT_DOC); 
		                   
		                        break;
		                    }

		                return true;
		            }
		        });

		        popup.show();
		    }
		});
	    
	 
		
		 
	    Save = (ImageView) findViewById(R.id.saveEditedStory);
	    Save.setOnClickListener(new OnClickListener() {
	                public void onClick(View arg0) { 
	            
	                	 new SaveNewStory().execute();
	         
	                }
	            });
		
		
		
	}
	
	
	private class GetDropDownvalues extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
			@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetPersons?" );		
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 		
		public void onCallBack(String result) {		

			if (result != null) {
				 
				try { 
					JSONArray array = new JSONArray(result);
					 data.clear();	
					 data1.clear();
					 data.add("null");
					 data1.add("null");

		                for(int a=0;a<array.length();a++){		                	    
			                    	 JSONObject p = array.getJSONObject(a);                      
                                    String Domain = p.getString("Domain");
			                    	String personName = p.getString("FullName"); 
			                    	    if(personName !="null"){
			                    	    	data.add( personName );
			                    	    }else if(Domain !="null"){
			                    	    	data1.add( Domain );         
			                    	    }
			                    	  			                            
		                }
		                
		                
		                ArrayAdapter<String> adapter = new ArrayAdapter<String>(NewStory.this, 
	                            android.R.layout.simple_list_item_1, data);
		              
		                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(NewStory.this, 
	                            android.R.layout.simple_list_item_1, data1);
		                
	                      spindrop.setAdapter(adapter); //narratedBy
	                      spindrop1.setAdapter(adapter);//collected By
	                      spindrop2.setAdapter(adapter1);//for domain 	                      
	                 
				} catch (JSONException e) {

					e.printStackTrace(); 				  
				}
			}  
			 
		} 

	}
	
	
	
	private class SaveNewStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();			
			 mProgressDialog = new ProgressDialog(NewStory.this);
			 mProgressDialog.setTitle("Processing....");
	         mProgressDialog.setMessage("Uploading StoryDetails....");	       
	         mProgressDialog.setCancelable(false);
	         mProgressDialog.show();			
		}
		@Override
 
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
 
					+"SaveNewStory?" );	
			
			
			SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);
		      
			 String h = settings.getString("Project", null); 
			 
			  // get selected radio button from radioGroup
	            int selectedId = radioGroup.getCheckedRadioButtonId();
	             
	            // find the radio button by returned id
	                radioButton = (RadioButton) findViewById(selectedId);       	                
				 
	  
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",getTitle.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("StoryTeller",spindrop.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("CollectedBy",spindrop1.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Domain",spindrop2.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Summary",getSummary.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("PreviousTitle",h)); 
			nameValuePairs.add(new BasicNameValuePair("FullText",getFullStory.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Status",(String) radioButton.getText())); 
			nameValuePairs.add(new BasicNameValuePair("AttachmnetType",Attachtype)); 
			nameValuePairs.add(new BasicNameValuePair("Attachmentname",Attachname)); 
			 
			 
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
 
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
 
		} 
		
		
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						resultMsg = o.getString("ResultMessage");		 
					 
						if (resultCode.equals("200")) {						
							 
							 new SendAttachment().execute();
							 
						} else if (resultCode.equals("201")) {			
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 
		 
		 
		} 	
      
	} 	

	
	private class SendAttachment extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		
		protected void onPreExecute() {
			super.onPreExecute();				 
	         mProgressDialog.setMessage("Uploading Attachment....");	       
	        			
		}
	 
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			 
			 
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetDoc?FullName=");				
		 
			File file = new File(filename);	 
			 
			
			InputStreamEntity reqEntity;
			try {
				reqEntity = new InputStreamEntity(new FileInputStream(file), -1);
				
				 reqEntity.setContentType("application/octet-stream");
				    reqEntity.setChunked(true); // Send in multiple parts if needed
				    httpPost.setEntity(reqEntity);
				  
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
			
			
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
 
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						 resultMsg = o.getString("ResultMessage");		 
				 

						if (resultCode.equals("200")) {			
							 						
							 Toast.makeText(getBaseContext(),resultMsg,Toast.LENGTH_SHORT).show();                            
							 
							 startActivity(new Intent(NewStory.this,Stories.class));						 
							 
							 
						} else if (resultCode.equals("201")) {
							
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 
			 
			mProgressDialog.dismiss(); 
		} 	
      
	} 	

	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	        if (requestCode == SELECT_PICTURE) {
	            Uri selectedImageUri = data.getData();
	            selectedImagePath = getPath(selectedImageUri);        
	             ImageView selectedImage = new ImageView(this);
	             RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(300, 300);
	             
	            // params.gravity= Gravity.CENTER;
	             params.addRule(RelativeLayout.CENTER_IN_PARENT); 
	             selectedImage.setLayoutParams(params);	 
	             
	             selectedImage.setImageURI(selectedImageUri);
	             
	             Rlayout.addView(selectedImage);
	             Attachtype = "image";                  
	             filename = selectedImagePath;
	             
	          
		            
	        }
	        else if(requestCode == SELECT_VIDEO){
	        	
                System.out.println("SELECT_VIDEO");
                Uri selectedVideoUri = data.getData();
                selectedVideoPath = getPath(selectedVideoUri);
                Attachtype = "video";   
                
                VideoView video = new VideoView(this);                
                RelativeLayout.LayoutParams fl = new RelativeLayout.LayoutParams(300, 300);
                fl.addRule(RelativeLayout.CENTER_IN_PARENT);
                video.setLayoutParams(fl);
                
                //Create video controllers
                MediaController mediacontroller = new MediaController(this);
                mediacontroller.setAnchorView(video);
                
                //Set controllers to video
                video.setMediaController(mediacontroller);
                video.setVideoURI(selectedVideoUri);
               // video.setVideoPath(selectedVideoPath);
                
                Rlayout.addView(video);
                
                filename = selectedVideoPath;
            
				   
            }
	        
	        else if(requestCode == SELECT_DOC){
	        	
	        	  System.out.println("SELECT_DOC");
	                Uri selectedDocumentUri = data.getData();
	                selectedDocumentPath = getPath(selectedDocumentUri);
	                Attachtype = "document";
	                Attachname = new File(selectedDocumentPath).getName();
	                filename = selectedDocumentPath;
	        	
	        }
	    }
	}

	public String getPath(Uri uri) {
	    String[] projection = { MediaStore.Images.Media.DATA };
	    @SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
	    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);
	}
	
 
 
}
