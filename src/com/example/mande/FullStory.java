package com.example.mande;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FullStory extends ActionBarActivity {
      String FullStory,action,p;
      Button Next, Back;
      Bundle bundle;
      EditText FullStoryEdit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_full_story);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
	    bundle = getIntent().getExtras();
		FullStory = bundle.getString("fullstory");	
		action = bundle.getString("Action");
		p = bundle.getString("ProjectName");
		  if(action.equalsIgnoreCase("newStory")){
			   TextView y = (TextView)findViewById(R.id.textView11);
			     y.setText("FULL STORY");
			  
		  }
		
	    FullStoryEdit= (EditText)findViewById(R.id.editText1);
	    FullStoryEdit.setText(FullStory);
	    
	    Next = (Button)findViewById(R.id.next1);
	    
	    
	    Next.setOnClickListener(new View.OnClickListener() 
        {		
			@Override
			public void onClick(View view) {
				Intent gotoAttachment = new Intent(FullStory.this,Attachment1.class);
				gotoAttachment.putExtra("fullstory", FullStoryEdit.getText().toString());
				gotoAttachment.putExtra("Action", bundle.getString("Action"));
				gotoAttachment.putExtra("storyname", bundle.getString("storyname"));
				gotoAttachment.putExtra("title", bundle.getString("title"));
				gotoAttachment.putExtra("narratedby", bundle.getString("narratedby"));
				gotoAttachment.putExtra("collectedby", bundle.getString("collectedby"));
				gotoAttachment.putExtra("status", bundle.getString("status"));
				gotoAttachment.putExtra("domain", bundle.getString("domain"));
				gotoAttachment.putExtra("summary",bundle.getString("summary"));
				gotoAttachment.putExtra("ProjectName",p); 
				
				
				 Toast.makeText(getBaseContext(), FullStoryEdit.getText()+ bundle.getString("storyname")+
						 bundle.getString("title")+bundle.getString("narratedby")+bundle.getString("collectedby")
						 +bundle.getString("status")+bundle.getString("domain")+bundle.getString("summary")+ 
						 "            "+bundle.getString("Action")+ p,
						 Toast.LENGTH_SHORT).show();
			    startActivity(gotoAttachment);	
			}
		});
	    
	   
	}


}
