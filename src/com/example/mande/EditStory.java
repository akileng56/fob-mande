package com.example.mande;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
 
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
 
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

 
import com.mande.activity.Stories;
 
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class EditStory extends Activity {
	String Storyname;
 
	ImageView Save;
	 ProgressDialog dialog = null;
	 
	RadioGroup radioGroup;
	RadioButton radioButton;
	String Title, Domain, Storyteller, CollectedBy, Summary, FullStory,filename;
	ProgressDialog pDialog, mProgressDialog;
	 String Attachname="",fullStory, send, Attachtype="null", title, strFile="", narratedby,imag ,image_str , collectedby, status, domain, summary,Action, projectName="";
	 String selectedImagePath, selectedDocumentPath, selectedVideoPath;
	 ImageView menubtn;
	private static final int SELECT_VIDEO = 2;
	    String selectedPath = "";   
	private static final int SELECT_DOC = 3;
	private static final int SELECT_PICTURE = 1;	 
	EditText getTitle, getSummary, getFullStory;
	Spinner spindrop,spindrop1,spindrop2;
	RelativeLayout Rlayout;
	int filelength = 0;
	 String upLoadServerUri = null;
 
	ArrayList<String> data = new ArrayList<String>();
	ArrayList<String> data1 = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		 
		
		setContentView(R.layout.activity_edit_story);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		Bundle bundle = getIntent().getExtras();
		Storyname = bundle.getString("name");
		radioGroup  = (RadioGroup)findViewById(R.id.radioGroup1);
		spindrop2 = (Spinner) findViewById(R.id.spinner1);
		spindrop = (Spinner) findViewById(R.id.spinner2);
	    getTitle = (EditText) findViewById(R.id.editText1);
	    getSummary = (EditText) findViewById(R.id.editText2);
		spindrop1 = (Spinner) findViewById(R.id.spinner3);
 
		 getFullStory = (EditText) findViewById(R.id.editfullStory);
		 Rlayout = (RelativeLayout)findViewById(R.id.middle2);
		 
         
		new GetDropDownvalues().execute();
		 
		
		
	    menubtn = ((ImageView) findViewById(R.id.Add));
		
		menubtn.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		        PopupMenu popup = new PopupMenu(EditStory.this, menubtn);
		        //Inflating the Popup using xml file
		        popup.getMenuInflater()
		                .inflate(R.menu.attachment1, popup.getMenu());

		        //registering popup with OnMenuItemClickListener
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {

		                switch (item.getItemId()){
		                    case R.id.image:
		                    	  Intent intent = new Intent();
		  	                      intent.setType("image/*");
		  	                      intent.setAction(Intent.ACTION_GET_CONTENT);
		  	                      startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
		                        break;
		                    case R.id.video:
		                   	      Intent intent1 = new Intent();
		                	      intent1.setAction(Intent.ACTION_GET_CONTENT);
		                          intent1.setType("video/*");                
		                          startActivityForResult(Intent.createChooser(intent1,"Select Video"), SELECT_VIDEO);
		                        break;
		                    case R.id.doc:
		                      /*  Intent intent2 = new Intent(EditStory.this,AttachDocument.class);
		                   	    intent2.putExtra("title", getTitle.getText().toString());
		                        startActivity(intent2);*/ 
		                    	
		                     Intent intentdoc = new Intent(Intent.ACTION_GET_CONTENT);
		                        intentdoc.setType("file/*");
		                        startActivityForResult(Intent.createChooser(intentdoc,"Select File"), SELECT_DOC); 		                   
		                        break;
		                 
		                    }

		                return true;
		            }
		        });

		        popup.show();
		    }
		});
	    
	 
		
		 
	    Save = (ImageView) findViewById(R.id.saveEditedStory);
	    Save.setOnClickListener(new View.OnClickListener() {
	                public void onClick(View arg0) { 
	                	
	                	Toast.makeText(getBaseContext(),new File(selectedImagePath).getName(),Toast.LENGTH_SHORT).show();   
	            
	                	 //new SaveEditedStory().execute();
	         
	                }
	            });
		
		 
 
	}
	
	
	private class GetDropDownvalues extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
			@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetPersons?" );		
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 		
		public void onCallBack(String result) {		
 

			if (result != null) {
				 
				try { 
					JSONArray array = new JSONArray(result);
					 data.clear();	
					 data1.clear();
					 data.add(" ");
					 data1.add(" ");

		                for(int a=0;a<array.length();a++){		                	    
			                    	 JSONObject p = array.getJSONObject(a);                      
                                    String Domain = p.getString("Domain");
			                    	String personName = p.getString("FullName"); 
			                    	    if(personName !="null"){
			                    	    	data.add( personName );
			                    	    }else if(Domain !="null"){
			                    	    	data1.add( Domain );         
			                    	    }
			                    	  			                            
		                }
		                
		                
		                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditStory.this, 
	                            android.R.layout.simple_list_item_1, data);
		              
		                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(EditStory.this, 
	                            android.R.layout.simple_list_item_1, data1);
		                
	                      spindrop.setAdapter(adapter); //narrated By
	                      spindrop1.setAdapter(adapter);//collected By
	                      spindrop2.setAdapter(adapter1);//for domain
	                      
	                      new RetrieveStoryToEdit().execute();
	                      
				} catch (JSONException e) {

					e.printStackTrace(); 				  
				}
			}  
			 
		} 

	}
	
	
	
	
	private class SaveEditedStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();			
			 mProgressDialog = new ProgressDialog(EditStory.this);
			 mProgressDialog.setTitle("Processing....");
	         mProgressDialog.setMessage("Uploading StoryDetails..");	         
	         mProgressDialog.setCancelable(false);	        
	         mProgressDialog.show();			
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"SaveEditedStory?" );
			
			
			    // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
             
               // find the radio button by returned id
                radioButton = (RadioButton) findViewById(selectedId);
	 
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",getTitle.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("StoryTeller",spindrop.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("CollectedBy",spindrop1.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Domain",spindrop2.getSelectedItem().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Summary",getSummary.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("PreviousTitle",Storyname)); 
			nameValuePairs.add(new BasicNameValuePair("FullText",getFullStory.getText().toString())); 
			nameValuePairs.add(new BasicNameValuePair("Status",(String) radioButton.getText())); 
			nameValuePairs.add(new BasicNameValuePair("AttachmnetType",Attachtype)); 
		    nameValuePairs.add(new BasicNameValuePair("Attachmentname",Attachname)); 
		
	 
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
 
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		
 
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						resultMsg = o.getString("ResultMessage");		
						 
						if (resultCode.equals("200")) {	
							
							 if(resultMsg.equalsIgnoreCase("No Attachment")){
								 
								 startActivity(new Intent(EditStory.this,Stories.class)); 
								 
							 }else{
								 
								 new SendAttachment().execute(); // Execute code for sending an Attachment	
							 }
						 
						} else if (resultCode.equals("201")) {
							
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 
		 
			 
		} 	
      
	} 
	
	
	
	
	private class SendAttachment extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();			 
	         mProgressDialog.setMessage("Uploading Attachment....");     			
		}
	 
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			 
			 
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetDoc?FullName=");	
			
		   
			File file = new File(filename);
	 
			InputStreamEntity reqEntity;
			try {
  
		            reqEntity = new InputStreamEntity(new FileInputStream(file),file.length());	
		           
					    reqEntity.setContentType("application/octet-stream");
					    reqEntity.setChunked(true); // Send in multiple parts if needed
					    
					    httpPost.setEntity(reqEntity);
	 
				  
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
			
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		 
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						 resultMsg = o.getString("ResultMessage");		 
				 

						if (resultCode.equals("200")) {			
							 						
							Toast.makeText(getBaseContext(),resultMsg,Toast.LENGTH_SHORT).show();                            
							 
							 startActivity(new Intent(EditStory.this,Stories.class));						 
							 
							 
						} else if (resultCode.equals("201")) {
							
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 	 			
				 
			 
				  mProgressDialog.dismiss(); 
		} 	
      
	} 	

	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	        if (requestCode == SELECT_PICTURE) {
	            Uri selectedImageUri = data.getData();
	            selectedImagePath = getPath(selectedImageUri);        
	             ImageView selectedImage = new ImageView(this);
	             RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(300, 300);
	            // params.gravity= Gravity.CENTER;
	             params.addRule(RelativeLayout.CENTER_IN_PARENT); 
	             selectedImage.setLayoutParams(params);	             
	             selectedImage.setImageURI(selectedImageUri);
	             Rlayout.addView(selectedImage);
	             Attachtype = "image";                  
	             filename = selectedImagePath;
	             
	          
		            
	        }
	        else if(requestCode == SELECT_VIDEO){
	        	
                System.out.println("SELECT_VIDEO");
                Uri selectedVideoUri = data.getData();
                selectedVideoPath = getPath(selectedVideoUri);
                Attachtype = "video";   
                
                VideoView video = new VideoView(this);                
                RelativeLayout.LayoutParams fl = new RelativeLayout.LayoutParams(300, 300);
                fl.addRule(RelativeLayout.CENTER_IN_PARENT);
                video.setLayoutParams(fl);
                
                //Create video controllers
                MediaController mediacontroller = new MediaController(this);
                mediacontroller.setAnchorView(video);
                
                //Set controllers to video
                video.setMediaController(mediacontroller);
                video.setVideoURI(selectedVideoUri);
               // video.setVideoPath(selectedVideoPath);
                
                Rlayout.addView(video);
                
                filename = selectedVideoPath;
            
				   
            }
	        
	        else if(requestCode == SELECT_DOC){	        	
	        	  System.out.println("SELECT_DOC");
	                Uri selectedDocumentUri = data.getData();
	                selectedDocumentPath = getPath(selectedDocumentUri);
	                Attachtype = "document";
	                Attachname = new File(selectedDocumentPath).getName();
	                filename = selectedDocumentPath;
	        	
	        }
	        
	      
	    }
	}

	public String getPath(Uri uri) {
	    String[] projection = { MediaStore.Images.Media.DATA };
	    @SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
	    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);
	}
	
	private class RetrieveStoryToEdit extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditStory.this);
			pDialog.setMessage("Loading......");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"StoryDetails?" );			

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",Storyname)); 
			 

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		
		
		public void onCallBack(String result) {			

 
			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						Title = o.getString("Title");
						Domain = o.getString("Domain");
						Storyteller = o.getString("StoryTeller");
						CollectedBy = o.getString("CollectedBy");
						Summary = o.getString("Summary");
						FullStory = o.getString("FullStory");
	                    
						
						getTitle.setText(Title);						
						
						spindrop2.setSelection(getIndex(spindrop2,Domain));						
					
						spindrop1.setSelection(getIndex(spindrop1,CollectedBy));						 
						
						spindrop.setSelection(getIndex(spindrop,Storyteller));			 
						
 
						getFullStory.setText(FullStory);
 
						getSummary.setText(Summary);
						
						
					} 
				} catch (JSONException e) {

					e.printStackTrace();  					
 				}

			}
			
			pDialog.dismiss();
		} 
 

	} 
	
	 private int getIndex(Spinner spinner, String myString)
	 {
	  int index = 0;

 
	  for (int i=0;i<spinner.getCount();i++){
	   if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
	    index = i;
	    break;
	   }
	  }
	  return index;
	 } 
 
}