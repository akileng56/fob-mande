package com.example.mande;

import java.util.ArrayList;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class NewStory_withTemplate extends ActionBarActivity {
	
	
	ArrayList<String> data = new ArrayList<String>();
	ArrayList<String> data1 = new ArrayList<String>(); 
	 ProgressDialog pDialog;
	 Spinner spindrop,spindrop1,spindrop2;	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_new_story_with_template);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		
		 	 
		 spindrop2 = (Spinner) findViewById(R.id.spinner1); //Domain
		 spindrop = (Spinner) findViewById(R.id.spinner2); // narratedby	    
		 spindrop1 = (Spinner) findViewById(R.id.spinner3); // collectedby
	      new GetDropDownvalues().execute();	
	}


	private class GetDropDownvalues extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
			@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetPersons?" );		
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 		
		public void onCallBack(String result) {		

			if (result != null) {
				 
				try { 
					JSONArray array = new JSONArray(result);
					 data.clear();	
					 data1.clear();
					 data.add("           ");
					 data1.add("          ");

		                for(int a=0;a<array.length();a++){		                	    
			                    	 JSONObject p = array.getJSONObject(a);                      
                                    String Domain = p.getString("Domain");
			                    	String personName = p.getString("FullName"); 
			                    	    if(personName !="null"){
			                    	    	data.add( personName );
			                    	    }else if(Domain !="null"){
			                    	    	data1.add( Domain );         
			                    	    }
			                    	  			                            
		                }
		                
		                
		                ArrayAdapter<String> adapter = new ArrayAdapter<String>(NewStory_withTemplate.this, 
	                            android.R.layout.simple_list_item_1, data);
		              
		                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(NewStory_withTemplate.this, 
	                            android.R.layout.simple_list_item_1, data1);
		                
	                      spindrop.setAdapter(adapter); //narratedBy
	                      spindrop1.setAdapter(adapter);//collected By
	                      spindrop2.setAdapter(adapter1);//for domain 	                      
	                 
				} catch (JSONException e) {

					e.printStackTrace(); 				  
				}
			}  
			 
		} 

	}
	
	
	public void onClick_questionlabel(View v) //Method called when u click the label Questions
	   {
	        startActivity(new Intent(NewStory_withTemplate.this,QuestionsEntry.class));
	   }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_story_with_template, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
