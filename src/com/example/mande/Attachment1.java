package com.example.mande;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost; 
import org.apache.http.entity.mime.MultipartEntity;
 
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.mande.activity.Login;
import com.mande.activity.MainMenuActivity;
import com.mande.activity.Stories;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

@SuppressLint("NewApi")
public class Attachment1 extends ActionBarActivity {
	 private static final int SELECT_VIDEO = 2;
	    String selectedPath = "";   
 
	    private static final int SELECT_DOC = 3;
 
	private static final int SELECT_PICTURE = 1;
    ProgressDialog pDialog;
	private String selectedImagePath;
	private ImageView img;
	  FileBody fileBody;
	  URL forimage;
	  ImageView menubtn;
	File imagee, video, doc;
	Button Browse, Save, document;
	static final int READ_BLOCK_SIZE = 100;
	protected static final int PICKFILE_RESULT_CODE = 0;
	TextView source;
	Bundle bundle;
    String fullStory, send,storyname, Attachtype="", title, strFile="", narratedby,imag ,image_str , collectedby, status, domain, summary,Action, projectName="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_attachment1);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		bundle = getIntent().getExtras();
		fullStory = bundle.getString("fullstory");	
		storyname = bundle.getString("storyname");
		title = bundle.getString("title");
		narratedby = bundle.getString("narratedby");
		collectedby = bundle.getString("collectedby");
		status = bundle.getString("status");
		domain = bundle.getString("domain");
		summary = bundle.getString("summary");		
		Action = bundle.getString("Action");	
		projectName = bundle.getString("ProjectName");
	    img = (ImageView)findViewById(R.id.ImageView01);
        source = (TextView)findViewById(R.id.textView1);
        
    
	 
	    
	    
		
	    menubtn = ((ImageView) findViewById(R.id.Add));
		
		menubtn.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		        PopupMenu popup = new PopupMenu(Attachment1.this, menubtn);
		        //Inflating the Popup using xml file
		        popup.getMenuInflater()
		                .inflate(R.menu.attachment1, popup.getMenu());

		        //registering popup with OnMenuItemClickListener
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {

		                switch (item.getItemId()){
		                    case R.id.help:
		                    	  Intent intent = new Intent();
		  	                      intent.setType("image/*");
		  	                      intent.setAction(Intent.ACTION_GET_CONTENT);
		  	                      startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
		                        break;
		                    case R.id.logout:
		                   	      Intent intent1 = new Intent();
		                	      intent1.setAction(Intent.ACTION_GET_CONTENT);
		                          intent1.setType("video/*");                
		                          startActivityForResult(Intent.createChooser(intent1,"Select Video"), SELECT_VIDEO);
		                        break;
		                    case R.id.settings:
		                        Intent intent2 = new Intent(Attachment1.this,AttachDocument.class);
		                   	    intent2.putExtra("title",title);
 
		                        startActivity(intent2); 
		                    	
		                      /*  Intent intentdoc = new Intent(Intent.ACTION_GET_CONTENT);
		                        intentdoc.setType("file/*");
		                        startActivityForResult(Intent.createChooser(intentdoc,"Select File"), SELECT_DOC);*/
		                   
		                        break;
		                    }
 

		                return true;
		            }
		        });

		        popup.show();
		    }
		});
	    
	    
	    
	    
	    
	    
	    Save = ((Button) findViewById(R.id.button1));
	    Save.setOnClickListener(new OnClickListener() {
	                public void onClick(View arg0) {
	                	if(Action.equalsIgnoreCase("edit")){
	                	new SaveEditedStory().execute();
	                	}else{
	                	new SaveNewStory().execute();	                		
	                	}
	                }
	            });
	    
  
	      }

 
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	        if (requestCode == SELECT_PICTURE) {
	            Uri selectedImageUri = data.getData();
	            selectedImagePath = getPath(selectedImageUri);
	            System.out.println("Image Path : " + selectedImagePath);
	            img.setImageURI(selectedImageUri);
	            source.setText(selectedImagePath);
	            
	            Attachtype = "image";                  
	            
	       	     Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath);
		           
		          ByteArrayOutputStream stream = new ByteArrayOutputStream();
		          
			     bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream); //compress to which format you want.
			      
		            byte [] byte_arr = stream.toByteArray();
		          //Encoding the Image to be Sent
		            strFile = Base64.encodeToString(byte_arr,Base64.DEFAULT);	
		        
		            
	        }
	        else if(requestCode == SELECT_VIDEO)
            {
                System.out.println("SELECT_VIDEO");
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                Attachtype = "video";
              
               File file = new File(selectedImagePath);
				
				byte[] videoData =  getFileAsBytes(file);	
				
				byte [] compressed = compress(videoData);
				
				strFile = Base64.encodeToString(compressed, Base64.DEFAULT);

				   
            }
	    }
	}

	
	
	public byte[] compress(final byte[] data) {
	    if (data == null || data.length == 0) return new byte[0];
	 
	    try (final ByteArrayOutputStream out = new ByteArrayOutputStream(data.length)) {
	        final Deflater deflater = new Deflater();        deflater.setInput(data); 
	        deflater.finish();
	        final byte[] buffer = new byte[1*1024*1024];
	        while (!deflater.finished()) {
	             out.write(buffer, 0, deflater.deflate(buffer));        }
	 
	        return out.toByteArray();
	    } catch (final IOException e) {
	        System.err.println("Compression failed! Returning the original data...");
	        e.printStackTrace();
	        return data;
	    }
	}
	
	
	
	public String getPath(Uri uri) {
	    String[] projection = { MediaStore.Images.Media.DATA };
	    @SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
	    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);
	}
	
 
    public static byte[] getFileAsBytes(File file) {
        byte[] bytes = null;
        InputStream is = null;
        try {
            is = new FileInputStream(file);

            // Get the size of the file
            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                Log.e("tag", "File " + file.getName() + "is too large");
                return null;
            }

            // Create the byte array to hold the data
            bytes = new byte[(int) length];

            // Read in the bytes
            int offset = 0;
            int read = 0;
            try {
                while (offset < bytes.length && read >= 0) {
                    read = is.read(bytes, offset, bytes.length - offset);
                    offset += read;
                }
            } catch (IOException e) {
                Log.e("tag", "Cannot read " + file.getName());
                e.printStackTrace();
                return null;
            }

            // Ensure all the bytes have been read in
            if (offset < bytes.length) {
                try {
                    throw new IOException("Could not completely read file " + file.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            return bytes;

        } catch (FileNotFoundException e) {
            Log.e("tag", "Cannot find " + file.getName());
            e.printStackTrace();
            return null;

        } finally {
            // Close the input stream
            try {
                is.close();
            } catch (IOException e) {
                Log.e("tag", "Cannot close input stream for " + file.getName());
                e.printStackTrace();
                return null;
            }
        }
    }
	
	
	private class SaveEditedStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();			
			pDialog = new ProgressDialog(Attachment1.this);
			pDialog.setMessage("Sending to Server...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
			
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"SaveEditedStory?" );				
 
	 
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",title)); 
			nameValuePairs.add(new BasicNameValuePair("StoryTeller",narratedby)); 
			nameValuePairs.add(new BasicNameValuePair("CollectedBy",collectedby)); 
			nameValuePairs.add(new BasicNameValuePair("Domain",domain)); 
			nameValuePairs.add(new BasicNameValuePair("Summary",summary)); 
			nameValuePairs.add(new BasicNameValuePair("PreviousTitle",storyname)); 
			nameValuePairs.add(new BasicNameValuePair("FullText",fullStory)); 
			nameValuePairs.add(new BasicNameValuePair("Status",status)); 
			nameValuePairs.add(new BasicNameValuePair("AttachmnetType",Attachtype)); 
			nameValuePairs.add(new BasicNameValuePair("Attachment", strFile));
		
 
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			 
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						resultMsg = o.getString("ResultMessage");		
						 
						if (resultCode.equals("200")) {						
							 						
							 Toast.makeText(getBaseContext(),resultMsg,Toast.LENGTH_SHORT).show();
							 
							 startActivity(new Intent(Attachment1.this,Stories.class));
							 
						} else if (resultCode.equals("201")) {
							
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 
			pDialog.dismiss();
			 
		} 	
      
	} 
	
	
	
	private class SaveNewStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();			
			pDialog = new ProgressDialog(Attachment1.this);
			pDialog.setMessage("Sending to Server...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
			
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"SaveNewStory?" );	
			
			
			SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);
		      
			 String h = settings.getString("Project", null); 
	  
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",title)); 
			nameValuePairs.add(new BasicNameValuePair("StoryTeller",narratedby)); 
			nameValuePairs.add(new BasicNameValuePair("CollectedBy",collectedby)); 
			nameValuePairs.add(new BasicNameValuePair("Domain",domain)); 
			nameValuePairs.add(new BasicNameValuePair("Summary",summary)); 
			nameValuePairs.add(new BasicNameValuePair("PreviousTitle",h)); 
			nameValuePairs.add(new BasicNameValuePair("FullText",fullStory)); 
			nameValuePairs.add(new BasicNameValuePair("Status",status)); 
			nameValuePairs.add(new BasicNameValuePair("AttachmnetType",Attachtype)); 
			nameValuePairs.add(new BasicNameValuePair("Attachment",strFile)); 
			 
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			 
			String resultCode, resultMsg;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						resultMsg = o.getString("ResultMessage");		 
						
						
						 

						if (resultCode.equals("200")) {						
							 						
							 Toast.makeText(getBaseContext(),resultMsg,Toast.LENGTH_SHORT).show();
							 
							 try {
								 FileOutputStream fileout=openFileOutput("Projectfile.txt", MODE_PRIVATE);
								 OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
								 outputWriter.write(projectName);
								 outputWriter.close();
								 
								 //display file saved message
								 Toast.makeText(getBaseContext(),projectName+" Saved successfully!",
								 Toast.LENGTH_SHORT).show();
								 
								 } catch (Exception e) {
								 e.printStackTrace();
								 }
							 
							 startActivity(new Intent(Attachment1.this,Stories.class));
							 
						} else if (resultCode.equals("201")) {
							
						 
							
						} 
					} 
				} catch (JSONException e) {
					e.printStackTrace(); 
	              
				}

			} else { 
				 
			} 
			pDialog.dismiss();
			 
		} 	
      
	} 	
	
 
	
}
