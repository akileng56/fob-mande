package com.example.mande;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.mande.activity.Login;
import com.mande.activity.Stories;
import com.mande.activity.StoryDetails;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;


public class MyStories extends ActionBarActivity {
	String s="";
	
ProgressDialog pDialog;

String storyname, Title;
static final int READ_BLOCK_SIZE = 100;
ImageView Addstory;
ListView listView;

ArrayList<String> data = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_my_stories);
		listView = (ListView)findViewById(R.id.MyStories);
		 
		
		new GetStories().execute();
	}

	
	
	private class GetStories extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MyStories.this);
			pDialog.setMessage("Retrieving Stories....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"RetrieveMyStories?" );
			/*
			//reading text from file
			 try {
			 FileInputStream fileIn=openFileInput("UserNamefile.txt");
			 InputStreamReader InputRead= new InputStreamReader(fileIn);
			 
			 char[] inputBuffer= new char[READ_BLOCK_SIZE];
			 
			 int charRead;
			 
			 while ((charRead=InputRead.read(inputBuffer))>0) {
			 // char to string conversion
			 String readstring=String.copyValueOf(inputBuffer,0,charRead);
			 s +=readstring; 
			 }
			 InputRead.close();
			 Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();
			 
			 } catch (Exception e) {
			 e.printStackTrace();
			 }
			 */
			
		 
		 
			SharedPreferences settings = getSharedPreferences(Login.LOGGED_IN_USER,0);
			      
			 String h = settings.getString("FullName", null);          // getting String
			 
			 
			// Toast.makeText(getBaseContext(), h,Toast.LENGTH_SHORT).show();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("FullName",h));				

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {		 

			if (result != null) 
				
			{
                try
                {
                    data.clear();
                    JSONArray array = new JSONArray(result);

                    for(int a=0;a<array.length();a++){
                    	    
   	                    	 JSONObject p = array.getJSONObject(a);
  	                      

   	                    	Title = p.getString("Title");
  	                            data.add(Title);  	                        
  	                            
  	                            
                    }
                    
                    if(data.size() > 0)
                    {
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyStories.this, 
                                android.R.layout.simple_list_item_1, data);

                        listView.setAdapter(adapter);
                        
                        listView.setOnItemClickListener(new OnItemClickListener() {
                            

							@SuppressLint("NewApi")
							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, final int position, long id) {
								// TODO Auto-generated method stub
								storyname = (String) parent.getItemAtPosition(position);
								AlertDialog.Builder builder = new AlertDialog.Builder(MyStories.this);
								
								builder.setTitle(storyname);
								
 
								builder.setItems(new CharSequence[] { "STORY DETAILS","EDIT STORY"},
 
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int which) {
												switch (which) {
												case 0:
													 //story details go here													
												     Intent gotoStoryDetails = new Intent(MyStories.this,StoryDetails.class);
												     gotoStoryDetails.putExtra("name", storyname);													     
												     startActivity(gotoStoryDetails);
												    
													break;
												case 1:
													//edit details go here
													  Intent gotoEditStory = new Intent(MyStories.this,EditStory.class);
													  gotoEditStory.putExtra("name", storyname);													     
													     startActivity(gotoEditStory);
													
													break;
																									 
												}
											}
										});
								builder.create().show();
							}
                        });
                        
                        
                        //put for Adding story
                        
                	/*	Addstory = (ImageView)findViewById(R.id.Add);
                		Addstory.setOnClickListener(new View.OnClickListener() 
                        {		
                			@Override
                			public void onClick(View view) {  
                				new CheckStory().execute();	                				
                				
                			}
                		});
                		*/
                		
                		
                		Addstory = ((ImageView) findViewById(R.id.MyStoriesMenu));
                		
                		Addstory.setOnClickListener(new View.OnClickListener() {
                		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                			@SuppressLint("NewApi")
                			@Override
                		    public void onClick(View v) {
                		        PopupMenu popup = new PopupMenu(MyStories.this, Addstory);
                		        //Inflating the Popup using xml file
                		        popup.getMenuInflater()
                		                .inflate(R.menu.my_stories, popup.getMenu());

                		        //registering popup with OnMenuItemClickListener                		        
                		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                		            public boolean onMenuItemClick(MenuItem item) {

                		                switch (item.getItemId()){
                		                 
                		                    case R.id.logout:
                		                    	startActivity(new Intent(MyStories.this,Login.class));                 		                   
                		                        break;
                		                       
                		                    case R.id.AddStory:
                		                    	SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);                		      			      
                		       				    String h = settings.getString("Project", null);                		       				    
                		       				 Intent gotoNewStory = new Intent(MyStories.this,NewStory.class);
         								    gotoNewStory.putExtra("ProjectName",h);
         							        startActivity(gotoNewStory);                		                   
                		                        break;  
                		                     

                		                }

                		                return true;
                		            }
                		        });

                		        popup.show();
                		    }
                		});
                	    
  
                        
                    }
                    else
                    {
                        Toast.makeText(MyStories.this, "No Stories Found", Toast.LENGTH_LONG).show();
                    }

                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
				else {				
				
				//Methods.showDialog(getSupportFragmentManager(), msg, title, ItecAlertDialog.DIALOG_OK_ACTION_NONE);
			} 				
			pDialog.dismiss();
		} 
	
}
	
	
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_stories, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
