package com.mande.tests;

import com.example.mande.R;
import com.mande.activity.Login;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class MyFirstTestActivityTest extends ActivityInstrumentationTestCase2<Login> {
	
	
	 private Login mFirstTestActivity;
	    private TextView mFirstTestText;
	    Intent mLaunchIntent;

	    public MyFirstTestActivityTest() {
	        super(Login.class);
	    }

	    
	    @Override
	    protected void setUp() throws Exception {
	        super.setUp();
	        
	        
	        mLaunchIntent = new Intent(getInstrumentation()
	                .getTargetContext(), Login.class);
	       

	        mFirstTestActivity = getActivity();
	        mFirstTestText =(TextView) mFirstTestActivity.findViewById(R.id.textViewFOBUrl);
	    }
	    
	     

	    public void testMyFirstTestTextView_labelText() {
	        final String expected =
	                mFirstTestActivity.getString(R.string.foburl);
	        final String actual = mFirstTestText.getText().toString();
	        assertEquals(expected, actual);
	    }
}
