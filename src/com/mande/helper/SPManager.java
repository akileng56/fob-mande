 
package com.mande.helper;

import android.content.SharedPreferences;

public class SPManager {

	public static final String KEY_LOGGED_IN_USER = "KEY_LOGGED_IN_USER";
	public static final String KEY_CLIENT_ID = "KEY_CLIENT_ID";
	public static final String KEY_CLIENT_IP_ADDRESS = "KEY_CLIENT_IP_ADDRESS";
	public static final String KEY_CLIENT_TYPE = "KEY_CLIENT_TYPE";
	public static final String KEY_CLIENT_COMPANY_NAME = "KEY_CLIENT_COMPANY_NAME";
	public static final String KEY_CLIENT_COUNTRY = "KEY_CLIENT_COUNTRY";
	public static final String KEY_CLIENT_COUNTRY_CODE_ISO = "KEY_CLIENT_COUNTRY_CODE_ISO";
	public static final String KEY_CLIENT_EMAIL_ADDRESS = "KEY_CLIENT_EMAIL_ADDRESS";
	public static final String KEY_CLIENT_EMAIL_OPT_IN = "KEY_CLIENT_EMAIL_OPT_IN";
	public static final String KEY_CLIENT_FIRST_NAME = "KEY_CLIENT_FIRST_NAME";
	public static final String KEY_CLIENT_LAST_NAME = "KEY_CLIENT_LAST_NAME";
	public static final String KEY_CLIENT_PHONE_NUMBER = "KEY_CLIENT_PHONE_NUMBER";
	public static final String KEY_CLIENT_PREFER_CONTACT_METHOD = "KEY_CLIENT_PREFER_CONTACT_METHOD";
	public static final String KEY_CLIENT_PREDER_LANGUAGE = "KEY_CLIENT_PREDER_LANGUAGE";
	public static final String KEY_CLIENT_REQUEST_ID = "KEY_CLIENT_REQUEST_ID";

	public static final String KEY_SIP_LOGIN = "KEY_SIP_LOGIN";
	public static final String KEY_SIP_PASSWORD = "KEY_SIP_PASSWORD";
	public static final String KEY_SIP_SERVER = "KEY_SIP_SERVER";
	public static final String KEY_SIP_PORT = "KEY_SIP_PORT";
	public static final String KEY_POCEKT_CLOUD_LOGIN = "KEY_POCEKT_CLOUD_LOGIN";
	public static final String KEY_POCEKT_CLOUD_PASSWORD = "KEY_POCEKT_CLOUD_PASSWORD";
	public static final String KEY_POCEKT_CLOUD_SERVER = "KEY_POCEKT_CLOUD_SERVER";
	public static final String KEY_POCEKT_CLOUD_PORT = "KEY_POCEKT_CLOUD_PORT";
	public static final String KEY_MINTLE_PASSWORD = "KEY_MINTLE_PASSWORD";
	public static final String KEY_ACT_GCM_ID = "KEY_ACT_GCM_ID";
	public static final String KEY_CB_USERNAME = "KEY_CB_USERNAME";
	public static final String KEY_CB_PASSWORD = "KEY_CB_PASSWORD";

	public static final String KEY_CELLNO = "KEY_CELLNO";
	public static final String KEY_APPID = "KEY_APPID";
	
	public static final String KEY_CB_Cell = "KEY_CB_CELL";
	public static final String KEY_CB_PIN = "KEY_CB_PIN";
	
	public static final String KEY_merchant_number = "merchant_number";
	public static final String KEY_merchant_pin = "merchant_pin";
	
	public static final String KEY_msisdn = "KEY_msisdn";
	public static final String KEY_pin = "KEY_pin";
	
	public static final String KEY_ISP = "KEY_ISP";
	public static final String KEY_ISP_PIN = "KEY_ISP_PIN";
	
	

	public static final String KEY_USERID_LOGIN = "KEY_USERID_LOGIN";
	public static final String KEY_PASSWORD_LOGIN = "KEY_PASSWORD_LOGIN";

	public static final String KEY_USERID_LOGIN_CHECK = "KEY_USERID_LOGIN_CHECK";
	public static final String KEY_PASSWORD_LOGIN_CHECK = "KEY_PASSWORD_LOGIN_CHECK";

	public static final String KEY_CALLBACK_ACCOUNT_NAME = "KEY_CALLBACK_ACCOUNT_NAME";
	public static final String KEY_CALLBACK_USERNAME = "KEY_CALLBACK_USERNAME";
	public static final String KEY_CALLBACK_PASSWORD = "KEY_CALLBACK_PASSWORD";

	public static final String KEY_ACTIVATED = "KEY_ACTIVATED";
	public static final String KEY_TOKEN_LOGIN = "KEY_TOKEN_LOGIN";
	public static final String KEY_ACT_CLIENT_ID = "KEY_ACT_CLIENT_ID";
	public static final String KEY_DIVISION_NAME = "KEY_DIVISION_NAME";
	public static final String KEY_MODULE_NAME = "KEY_MODULE_NAME";
	public static final String KEY_SETTING_NAME = "KEY_SETTING_NAME";
	public static final String KEY_SETTING_DATA = "KEY_SETTING_DATA";
 
	
	public static final String KEY_CLOUDBANC_USERNAME = "cloudbancUsername";
	public static final String KEY_CLOUDBANC_PASSWORD = "cloudbancPassword";
	public static final String KEY_CLOUDBANC_ACTIVATION_STATUS = "cloudbancActivationStatus";
 
		Methods m = new Methods();
	public static void save(String key, String data) {

		SharedPreferences.Editor editor = Methods.getS().edit();

		editor.putString(key, data);
		editor.commit();

	}

	public static void saveInt(String key, int data) {

		SharedPreferences.Editor editor = Methods.getS().edit();

		editor.putInt(key, data);
		editor.commit();

	}

	public static void saveBoolean(String key, boolean data) {

		SharedPreferences.Editor editor = Methods.getS().edit();

		editor.putBoolean(key, data);
		editor.commit();

	}

	public static void delete(String key) {
		SharedPreferences.Editor editor = Methods.getS().edit();

		editor.putString(key, "null");
		editor.commit();
	}

	public static String retrive(String key) {
		return Methods.getS().getString(key, "");
	}
	
	public static boolean retriveBoolean(String key) {
		return Methods.getS().getBoolean(key, false);
	} 

	public static int retriveInt(String key) {
		return Methods.getS().getInt(key, 0);
	}

	public static boolean exists(String key) {

		if (Methods.getS().getString(key, "null").equals("null")) {
			return false;
		}

		return true;
	}

	public static void save(String key, boolean value) {
		SharedPreferences.Editor editor = Methods.getS().edit();

		editor.putBoolean(key, value);
		editor.commit();
	}
}
