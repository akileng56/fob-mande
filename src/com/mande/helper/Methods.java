/**
 * @author  
 * Mumbai, India
 */
package com.mande.helper;

 
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.telephony.TelephonyManager;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;  

public class Methods extends Application{

	private	static Application App ;
	private static TelephonyManager tManager;
	private static Gson gson;
	static SharedPreferences s;
	
	public Methods() {
		// TODO Auto-generated constructor stub
		 s= getSharedPreferences("Sapientlite", Context.MODE_PRIVATE);
	}
	

	 
	
	@Override
	public void onCreate() {
		App = this;
		tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		gson = new Gson();
		super.onCreate();
	}
	
	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}
	
	public final static Gson getGson(){
		return gson;
	}
	
	public static Application getInstance(){
		return App;
	}
	public static SharedPreferences getS() {
		return s;
	}
	
/*	public static void showDialog(FragmentManager manager, String message,
			String title, int dialogACtion) {
		Bundle args = new Bundle();
		args.putString(Values.KEY_MESSAGE, message);
		args.putString(Values.KEY_TITLE, title);

		ItecAlertDialog itecAlertDialog = new ItecAlertDialog();
		itecAlertDialog.setArguments(args);
		itecAlertDialog.showDialog(manager, message, dialogACtion);
	}*/
	
	/*public static ShowDialogActivation dialog(){
		return new ShowDialogActivation();
	}*/ 
	
/*	public static class ShowDialogActivation{
		String message = "00:30";
		ItecAlertDialog itecAlertDialog;
		
		public void setMessage(String message) {
			itecAlertDialog.setTextMessage(message);
		}
		
		public void showDialog(String title,FragmentManager manager){
			Bundle args = new Bundle();
			args.putString(Values.KEY_MESSAGE, message);
			args.putString(Values.KEY_TITLE, title);
			itecAlertDialog = new ItecAlertDialog();
			itecAlertDialog.setArguments(args);
			itecAlertDialog.showDialog(manager, message, ItecAlertDialog.DIALOG_CANCEL_ACTION_ON_BACK_PRESS);
		}
		
		public void dismiss(){
			itecAlertDialog.dismiss();
		}
	}*/

//	public static boolean isLoggedIn(Context c) {
//		if (SPManager.exists(SPManager.KEY_USERID_LOGIN_CHECK)
//				&& SPManager.exists(SPManager.KEY_PASSWORD_LOGIN_CHECK)) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	public static boolean isPackageExists(String targetPackage, Context c) {
		PackageManager pm = c.getPackageManager();
		try {
			pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			return false;
		}
		return true;
	}

//	public static boolean hasRegisteredToDell(Context context) {
//
//		return SPManager.exists(SPManager.KEY_CLIENT_ID);
//	}

	public static boolean isNotBlank(EditText txt) {
		if (txt.length() > 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isConnected() {
		ConnectivityManager connec = (ConnectivityManager) App.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connec != null) {
			android.net.NetworkInfo wifi = connec
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			android.net.NetworkInfo mobile = connec
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (wifi.isConnected()) {
				return true;
			} else {
				if (mobile != null) {
					if (mobile.isConnected()) {
						return true;
					} else {
						return false;
					}
				}
			}
		}

		return false;
	}

	public static boolean hasGooglePlay() {

		int i = GooglePlayServicesUtil.isGooglePlayServicesAvailable(App);

		if (ConnectionResult.SUCCESS == i) {
			return true;
		}

		return false;
	}

	public static String getAppPackageName() {

		return App.getPackageName();
	}

	public static String getAppVersion() {

		try {
			return App.getPackageManager()
					.getPackageInfo(App.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();

			return "0";
		}
	}

	public static String getDeviceManufacturer() {

		return Build.MANUFACTURER;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("InlinedApi")
	public static String getDeviceSerialNumber() {

		return Build.SERIAL;
	}

	public static String getDeviceModel() {

		return Build.MODEL;
	}

	public static String getDeviceIMEI() {
		return tManager.getDeviceId();
	}

/*	public static String getDeviceOS() {
		return Values.OS_ANDROID;
	}*/

	public static String getCarrier() {

		return tManager.getSimOperatorName();
	}

	public static String getCountryCode() {

		return tManager.getSimCountryIso();
	}
}