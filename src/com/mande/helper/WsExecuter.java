package com.mande.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;
import android.util.Log;


public class WsExecuter {	

	// Call back URL for making calls
	public static final String BASE_URL_CALLBACK = "http://196.38.58.252:8080/ursa/rest/";

	// LIVE

	public static final String BASE_URL = "http://192.168.1.10:8089/rest/";

	//public static final String BASE_URL = "http://flockofbirds.fwd.wf/rest/";
	
	public static final String HELP_URL = "http://154.65.52.77/rest/";

	public static final String XML_URL = "http://sapienteducation.blogspot.com/feeds/posts/default?alt=json";
	//public static final String XML_URL = "http://alldayapp.blogspot.com//feeds/posts/default?alt=json";
	public static final String AND = "&";
	public static final String QUE = "?";
	public static final String EQU = "=";

	public static final int TYPE_GET = 1;
	public static final int TYPE_POST = 2;

	public static final String GCM_SENDER_ID = "880412210461";// "825835691328";

	private static ServiceHitListner listner;

	private HttpPost httpPost;
	private String url;
	private int type;

	public WsExecuter(HttpPost httpPost, String url, int type,
			ServiceHitListner listner) {
		DebugLogConfig.enable();
		WsExecuter.listner = listner;
		this.httpPost = httpPost;
		this.url = url;
		this.type = type;
		DebugLogConfig.enable();
		(new AsyncExecuter()).execute();
	}

	public static class Activate {

		public static final String SERVICE = "ActivateEmployee";

		public static final String PARAM_APP_ID = "aid";
		public static final String PARAM_USERID = "uid";
		public static final String PARAM_PASSWORD = "pw";
		public static final String PARAM_CELL_NO = "cno";

		public static final String PARAM_APP_VERSION = "av";
		public static final String PARAM_APP_PACKAGE_NAME = "apn";
		public static final String PARAM_DEVICE_MODEL = "dm";
		public static final String PARAM_DEVICE_IMEI = "im";
		public static final String PARAM_DEVICE_MANUFACTURER = "dmf";
		public static final String PARAM_DEVICE_SERIAL_NO = "dsn";
		public static final String PARAM_DEVICE_OS = "dos";
		public static final String PARAM_CARRIER = "cr";
		public static final String PARAM_COUNTRY_CODE = "cc";

		public static final String RESP_RESULT_CODE = "ResultCode";
		public static final String RESP_RESULT_MESSAGE = "ResultMessage";
		public static final String RESP_TOKEN = "Token";
		public static final String RESP_ACTIVATED = "Activated";
		public static final String RESP_CLIENT_ID = "ClientID";
		public static final String RESP_DIVISION_NAME = "DivisionName";
		public static final String RESP_MODULE_NAME = "ModuleName";
		public static final String RESP_SETTING_NAME = "SettingName";
		public static final String RESP_SETTING_DATA = "SettingData";
	}

	public static class ForgotPassword {

		public static final String SERVICE = "ForgotPW";

		public static final String PARAM_USERID = "uid";
		public static final String PARAM_CELL_NO = "cno";
		public static final String PARAM_CLIENT_ID = "cid";
		public static final String PARAM_APP_ID = "aid";

		public static final String RESP_RESULT_CODE = "ResultCode";
		public static final String RESP_RESULT_MESSAGE = "ResultMessage";

	}

	public static class Authentication {

		public static final String SERVICE = "Authentication";
		
		public static final String PARAM_USERID = "uid";
		public static final String PARAM_PASSWORD = "pw";
		public static final String PARAM_CLIENT_ID = "cid";
		public static final String PARAM_APP_ID = "aid";
		public static final String PARAM_APP_PACKAGE_NAME = "apn";
		public static final String PARAM_APP_APP_VERSION = "av";

	}

	public static class Login {

		public static final String SERVICE ="getuser";
		
		public static final String PARAM_Username = "Username";
		public static final String PARAM_Password = "Password";


	}

	public static class UpdatePassword {

		public static final String SERVICE = "UpdatePW";

		public static final String PARAM_NEW_PASSWORD = "pw";
		public static final String PARAM_OLD_PASSWORD = "opw";
		public static final String PARAM_USERID = "uid";
		public static final String PARAM_TOKEN = "tk";
		public static final String PARAM_CLIENT_ID = "cid";
		public static final String PARAM_APP_ID = "aid";

		public static final String RESP_RESULT_CODE = "ResultCode";
		public static final String RESP_RESULT_MESSAGE = "ResultMessage";
	}

	public static class CallBack {

		public static final String SERVICE = "cb";

		public static final String PARAM_APP_ID = "aid";
		public static final String PARAM_DATE_TYPE = "dt";
		public static final String PARAM_ACC_NAME = "ac";
		public static final String PARAM_USERNAME = "un";
		public static final String PARAM_PASSWORD = "pw";
		public static final String PARAM_CELL_FROM = "cnf";
		public static final String PARAM_CELL_TO = "cnt";
		public static final String PARAM_TOKEN = "tk";
		public static final String PARAM_CLIENT_ID = "cid";
		public static final String PARAM_USER_ID = "uid";
	}

	public static class ErrorLog {
		public static final String SERVICE = "Rest_Logging";

		public static final String PARAM_LOG_CODE = "LOGCODE";
		public static final String PARAM_MESSAGE_TEXT = "LOGMESSAGE";
		public static final String PARAM_LOG_TYPE = "CREATEDDATE";
		public static final String PARAM_LOG_DATE_TYPE = "LOGDATE";
		public static final String PARAM_CLIENT_ID = "CLIENTID";

		public static final String PARAM_PROGRAM_NAME = "PROGRAMNAME";
		public static final String PARAM_TOKEN = "TOKEN";
		public static final String PARAM_APP_ID = "APPID";
		public static final String PARAM_USERID = "USERID";

		public static final String PARAM_EXTRA1 = "EXTRALOG1";
		public static final String PARAM_EXTRA2 = "EXTRALOG2";
		public static final String PARAM_EXTRA3 = "EXTRALOG3";
		public static final String PARAM_EXTRA4 = "EXTRALOG4";
		public static final String PARAM_EXTRA5 = "EXTRALOG5";
	}

	public static class SelfCare {

		public static final String SERVICE = "Rest_Logging";

		public static final String PARAM_USERID = "uid";
		public static final String PARAM_DATE = "dt";
		public static final String PARAM_SELF_CARE_TYPE = "sctp";
		public static final String PARAM_PRODUCT = "prd";
		public static final String PARAM_DESCRIPTION = "dsc";
		public static final String PARAM_OTHER_1 = "oth1";
		public static final String PARAM_OTHER_2 = "oth2";
		public static final String PARAM_TOKEN = "tk";
		public static final String PARAM_CLIENT_ID = "cid";
		public static final String PARAM_APP_ID = "aid";
	}

	public static class RegisterDellClient {

		public static final String SERVICE = "dll/rdc";

		public static final String PARAM_CLIENT_HOSTNAME = "clientHostName";
		public static final String PARAM_CLIENT_ID = "clientID";
		public static final String PARAM_CLIENT_IP_ADDRESS = "clientIpAddress";
		public static final String PARAM_CLIENT_TYPE = "clientType";
		public static final String PARAM_CLIENT_COMPANY_NAME = "companyName";
		public static final String PARAM_CLIENT_COUNTRY = "country";
		public static final String PARAM_CLIENT_COUNTRY_CODE_ISO = "countryCodeISO";
		public static final String PARAM_CLIENT_EMAIL_ADDRESS = "emailAddress";
		public static final String PARAM_CLIENT_EMAIL_OPT_IN = "emailOptIn";
		public static final String PARAM_CLIENT_FIRST_NAME = "firstName";
		public static final String PARAM_CLIENT_LAST_NAME = "lastName";
		public static final String PARAM_CLIENT_PHONE_NUMBER = "phoneNumber";
		public static final String PARAM_CLIENT_PREFER_CONTACT_METHOD = "preferContactMethod";
		public static final String PARAM_CLIENT_PREDER_LANGUAGE = "preferLanguage";
		public static final String PARAM_CLIENT_REQUEST_ID = "requestId";
	}

	public static class CreateServiceRequest {

		public static final String SERVICE = "dll/csr";

		public static final String PARAM_EVENT_ID = "eventId";
		public static final String PARAM_TRAP_ID = "trapId";
		public static final String PARAM_EVENT_SOURCE = "eventSource";
		public static final String PARAM_SEVERITY = "severity";
		public static final String PARAM_CASE_SEVERITY = "caseSeverity";
		public static final String PARAM_MESSAGE = "message";
		public static final String PARAM_SERVICE_TAG = "serviceTag";
		public static final String PARAM_DEVICE_NAME = "deviceName";
		public static final String PARAM_DEVICE_IP = "deviceIp";
		public static final String PARAM_DEVICE_MODEL = "deviceModel";
		public static final String PARAM_DEVICE_TYPE = "deviceType";
		public static final String PARAM_OS = "os";
		public static final String PARAM_DIAGNOS_OPT_IN = "diagnosticsOptIn";
	}

	public static class ServiceTag {

		public static final String SERVICE_URL = "https://api.dell.com/support/assetinfo/v2/summary.json";

		public static final String PARAM_SV_TAGS = "svctags";
		public static final String PARAM_API_KEY = "apikey";

		public static final String KEY_ASSET_SUMMARY_RESPONSE = "GetAssetSummaryResponse";
		public static final String KEY_ASSET_SUMMARY_RESULT = "GetAssetSummaryResult";
		public static final String KEY_RESPONSE = "Response";
		public static final String KEY_DELL_ASSET = "DellAsset";
		public static final String KEY_MOBILE_APP_FAMILY = "MobileAppProductFamily";
		public static final String KEY_MACHINE_DESCRIPTION = "MachineDescription";
		public static final String KEY_IMAGE_URL = "ImageUrl";
		public static final String KEY_WARRANTIES = "Warranties";
		public static final String KEY_WARRANTY = "Warranty";
		public static final String KEY_END_DATE = "EndDate";

		public static final String KEY_MOBILE_APP_PRODUCT_LINE = "MobileAppProductLine";
		public static final String KEY_PRODUCT_DESCRIPTION = "ProductDescription";
		public static final String KEY_PRODUCT_FAMILY = "ProductFamily";
		public static final String KEY_SHIP_DATE = "ShipDate";
		public static final String KEY_ORDER_NUMBER = "OrderNumber";

		public static final String KEY_ASSET_PART = "AssetPart";
		public static final String KEY_ASSET_PARTS = "AssetParts";
		public static final String KEY_PART_DESCRIPTION = "PartDescription";
		public static final String KEY_PART_DESCRIPTION_REFINED = "PartDescriptionRefined";
		public static final String KEY_PART_NUMBER = "PartNumber";
		public static final String KEY_QUANTITY = "Quantity";
		public static final String KEY_SKU_NUMBER = "SkuNumber";

		public static final String KEY_START_DATE = "StartDate";
		public static final String KEY_ENTITLEMENT_TYPE = "EntitlementType";
		public static final String KEY_SERVICE_LEVEL_CODE = "ServiceLevelCode";
		public static final String KEY_SERVICE_LEVEL_DESCRIPTION = "ServiceLevelDescription";
		public static final String KEY_SERVICE_LEVEL_GROUP = "ServiceLevelGroup";
		public static final String KEY_SERVICE_PROVIDER = "ServiceProvider";

		public static final String KEY_ASSET_PART_ARRAY = "AssetPartArray";
		public static final String KEY_WARRANTY_ARRAY = "WarrantyArray";

	}

	public class AsyncExecuter extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {

			StringBuilder str;

			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 10000;
			HttpConnectionParams.setConnectionTimeout(httpParams,
					timeoutConnection);

			int timeoutSocket = 15000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			HttpClient httpClient = new DefaultHttpClient(httpParams);
			HttpResponse httpResponse = null;
			str = new StringBuilder();
			try {
				if (type == TYPE_GET) {

					Log.v("TAG", "Url for get: " + url);

					httpResponse = httpClient.execute(new HttpGet(url));
				} else if (type == TYPE_POST) {

					Log.v("TAG", "Url for post: " + url);

					httpResponse = httpClient.execute(httpPost);
				}

				Log.v("TAG", "Raw Response: " + httpResponse);

				InputStreamReader streamReader = new InputStreamReader(
						httpResponse.getEntity().getContent());
				BufferedReader br = new BufferedReader(streamReader);
				String line;

				while ((line = br.readLine()) != null) {
					str.append(line);
				}

			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			try {
				Log.v("TAG", "Response: " + str.toString());
				return str.toString();
			} catch (Exception e) {
				return null;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			listner.onCallBack(result);
		}
	}
}
