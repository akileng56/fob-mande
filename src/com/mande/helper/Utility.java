package com.mande.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.mande.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
/**
 * Class which has Utility methods
 * 
 */
public class Utility {
	
	
	public static final String KEY_MESSAGE = "key_message";
    public static final String KEY_TITLE = "key_title";
    public static final String MyPREFERENCES = "mandeInfo";
    public static final String USER_OBJECT_KEY = "user_object";
    public static final String USER_TOKEN_KEY = "TokenKey";
 
        public static final String KEY_APP_VERSION_NO = "1.0";
        public static final String KEY_USERID_LOGIN = "uid";
        public static final String KEY_APPID = "aid";
        public static final String KEY_TOKEN_LOGIN = "tk";
        public static final String KEY_ACT_CLIENT_ID = "cid";

        public static final String KEY_STYLE_ELEMENT = "Theme.AppCompat.Dark";
        public static final String KEY_STYLE_ELEMENT_DARK = "Theme.AppCompat.Light";

        public static final int WS_UPDATE_PASS = 4;
    static AlertDialog alert = null;
	
    
    
    
    public static void save(String key,String value,Activity givenActivity){
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString(key, value).commit();

    }


    public static void saveUser(User user, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(user);
        Log.e("user info", "" + json);
        prefs.edit().putString(USER_OBJECT_KEY, json).commit();
    } 
    
    public static SharedPreferences getSharedPreferencesWithin(Activity givenActivity) {
        return givenActivity.getSharedPreferences(
                MyPREFERENCES, Context.MODE_PRIVATE);
    }
    
    public static void saveToken(String token, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString(USER_TOKEN_KEY, token).commit();
    }


    public static String getAuthorizationToken(Context activity) {
        SharedPreferences prefs = activity.getSharedPreferences(
                Utility.MyPREFERENCES, Context.MODE_PRIVATE);
        String token = prefs.getString(Utility.USER_TOKEN_KEY, "");
        return String.format("Token %s", token);
    }


    public static User getUser(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = prefs.getString(USER_OBJECT_KEY, "");
        Log.e("wat is got", "" + json);
        return gson.fromJson(json, User.class);
    }

    public static String getSaved(String key,Activity givenActivity){
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        String js = prefs.getString(key, "");

        return js;

    }


    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


    public static String ifExits(Context activity) {
        SharedPreferences prefs = activity.getSharedPreferences(
                Utility.MyPREFERENCES, Context.MODE_PRIVATE);
        String token = prefs.getString(Utility.USER_TOKEN_KEY, "");
        return token;
    }

    @SuppressLint("NewApi")
	public static void deleteAuthorizationToken(Context activity) {
        SharedPreferences prefs = activity.getSharedPreferences(
                Utility.MyPREFERENCES, Context.MODE_PRIVATE);
        prefs.edit().putString(Utility.USER_TOKEN_KEY, "").apply();
        prefs.edit().commit();

    }

    @SuppressLint("NewApi")
	public static void deleteUser(Context activity) {
        SharedPreferences prefs = activity.getSharedPreferences(
                Utility.MyPREFERENCES, Context.MODE_PRIVATE);
        prefs.edit().putString(Utility.USER_OBJECT_KEY, "").apply();
        prefs.edit().commit();

    }
    
	private static Pattern pattern;
	private static Matcher matcher;
	//Email Pattern
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	/**
	 * Validate Email with regular expression
	 * 
	 * @param email
	 * @return true for Valid Email and false for Invalid Email
	 */
	public static boolean validate(String email) {
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();
 
	}
	/**
	 * Checks for Null String object
	 * 
	 * @param txt
	 * @return true for not null and false for null String object
	 */
	public static boolean isNotNull(String txt){
		return txt!=null && txt.trim().length()>0 ? true: false;
	}
}
