package com.mande.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mande.dialog.MandeAlertDialog;
import com.mande.helper.Utility;

/**
 * Created by User on 13/03/2015.
 */
public class Method extends ActionBarActivity {

    public final static String OS_ANDROID = "Android";

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }





    public static void showDialog(android.support.v4.app.FragmentManager manager, String message,
                                  String title, int dialogACtion) {
        Bundle args = new Bundle();
        args.putString(Utility.KEY_MESSAGE, message);
        args.putString(Utility.KEY_TITLE, title);

        MandeAlertDialog MandeAlertDialog = new MandeAlertDialog();
        MandeAlertDialog.setArguments(args);
        MandeAlertDialog.showDialog(manager, message, dialogACtion);
    }


    public static ShowDialogActivation dialog(){
        return new ShowDialogActivation();
    }

    public static class ShowDialogActivation{
        String message = "00:30";
        MandeAlertDialog MandeAlertDialog;

        public void setMessage(String message) {
        	MandeAlertDialog.setTextMessage(message);
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void showDialog(String title,FragmentManager manager){
            Bundle args = new Bundle();
            args.putString(Utility.KEY_MESSAGE, message);
            args.putString(Utility.KEY_TITLE, title);
            MandeAlertDialog = new MandeAlertDialog();
            MandeAlertDialog.setArguments(args);
            MandeAlertDialog.showDialog(manager, message, MandeAlertDialog.DIALOG_CANCEL_ACTION_ON_BACK_PRESS);
        }
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void dismiss(){
        	MandeAlertDialog.dismiss();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void showDialogCustomButtons(FragmentManager manager,
                                               String message, String title, int dialogACtion, String okButton,
                                               String functionButton) {
        Bundle args = new Bundle();
        args.putString(Utility.KEY_MESSAGE, message);
        args.putString(Utility.KEY_TITLE, title);

        MandeAlertDialog MandeAlertDialog = new MandeAlertDialog();
        MandeAlertDialog.setArguments(args);
        MandeAlertDialog.setOkText(okButton);
        MandeAlertDialog.setCancelText(functionButton);
        MandeAlertDialog.setCancelable(false);
        MandeAlertDialog.showDialog(manager, message, dialogACtion);
    }



    public static boolean isConnected(Context c) {
        ConnectivityManager connec = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connec != null) {
            android.net.NetworkInfo wifi = connec
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = connec
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (wifi.isConnected()) {
                return true;
            } else {
                if (mobile != null) {
                    if (mobile.isConnected()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return false;
    }


    public static boolean hasGooglePlay(Context c) {

        int i = GooglePlayServicesUtil.isGooglePlayServicesAvailable(c);

        if (ConnectionResult.SUCCESS == i) {
            return true;
        }

        return false;
    }

    public static String getAppPackageName(Context c) {

        return c.getPackageName();
    }

    public static String getAppVersion(Context c) {

        try {
            return c.getPackageManager()
                    .getPackageInfo(c.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

            return "0";
        }
    }

    public static String getDeviceManufacturer() {

        return Build.MANUFACTURER;
    }

    @SuppressLint("NewApi")
	public static String getDeviceSerialNumber() {

        return Build.SERIAL;
    }

    public static String getDeviceModel() {

        return Build.MODEL;
    }

    public static String getDeviceIMEI(Context c) {

        TelephonyManager tManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);


        return tManager.getDeviceId();
    }

    public static String getDeviceOS() {

        return OS_ANDROID;
    }

    public static String getCarrier(Context c) {
        TelephonyManager tManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);

        return tManager.getSimOperatorName();
    }

    public static String getCountryCode(Context c) {
        TelephonyManager tManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);

        return tManager.getSimCountryIso();
    }

}
