package com.mande.dialog;

import com.example.mande.R;
import com.mande.helper.Utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by User on 13/03/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MandeAlertDialog extends DialogFragment {

    private TextView txtMessage;
    private TextView txtTitle;
    private ImageButton btnCancel;
    private Button btnOk, btnOk_close, btn_ticket;
    private int dialogAction = DIALOG_OK_ACTION_NONE;
    private LinearLayout llmultibutton;

    public static final int DIALOG_OK_ACTION_NONE = 0;
    public static final int DIALOG_OK_ACTION_ON_BACK_PRESS = 4;
    public static final int DIALOG_OK_ACTION_START_PLAY_STORE_APP = 1;
    public static final int DIALOG_LOADING = 5;
    public static final int DIALOG_CANCEL_ACTION_NONE = 2;
    public static final int DIALOG_CANCEL_ACTION_ON_BACK_PRESS = 3;
    public static final int DIALOG_OK_ACTION_TICKETS_CALL = 6;
    public static final int DIALOG_OK_ACTION_TOP_BALANCE = 7;
    public static final int DIALOG_OK_ACTION_DONE=8;


    public interface MandeDialogListener {
        public void onDialogOkClick(DialogFragment dialog);

        public void onDialogDismiss(DialogFragment dialog);

        public void onDialogOk2Click(DialogFragment dialog);
    }

    public void setTextMessage(String message) {
        txtMessage.setText(message);
    }
    // Use this instance of the interface to deliver action events
    MandeDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            mListener = (MandeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        onDialogButtonClick(0);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.dialog_alert, null);
        txtMessage = (TextView) v.findViewById(R.id.txtAlertMeassage);
        txtTitle = (TextView) v.findViewById(R.id.txt_alert_title);

        Spanned htmlText = Html.fromHtml(getArguments().getString(
                Utility.KEY_MESSAGE));
        txtTitle.setText(getArguments().getString(Utility.KEY_TITLE));
        txtMessage.setText(htmlText, TextView.BufferType.SPANNABLE);
        btnCancel = (ImageButton) v.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onDialogButtonClick(0);

            }
        });
        if (showOk() == 1) {
            btnOk = (Button) v.findViewById(R.id.btn_ok_itecdialog);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    onDialogButtonClick(1);
                }
            });
        } else if (showOk() == 2) {

            llmultibutton = (LinearLayout) v.findViewById(R.id.llmultibutton);
            llmultibutton.setVisibility(View.VISIBLE);

            btnOk_close = (Button) v.findViewById(R.id.btn_ok_itecdialog_1);

            btn_ticket = (Button) v.findViewById(R.id.btn_ok_itecdialog_2);

            btnOk_close.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    onDialogButtonClick(1);
                }
            });

            btn_ticket.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onDialogButtonClick(2);
                }
            });
        }else if (showOk() == 3){
            llmultibutton = (LinearLayout) v.findViewById(R.id.llmultibutton);
            llmultibutton.setVisibility(View.VISIBLE);

            btnOk_close = (Button) v.findViewById(R.id.btn_ok_itecdialog_1);

            btn_ticket = (Button) v.findViewById(R.id.btn_ok_itecdialog_2);
            btn_ticket.setText("Refill");
            btnOk_close.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    onDialogButtonClick(1);
                }
            });

            btn_ticket.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onDialogButtonClick(2);
                }
            });
        }else if(showOk() == 8){
            onDialogButtonClick(8);
        }

        builder.setView(v);
        // Add action buttons

        return builder.create();
    }


    private int showOk() {
        int showOk = 0;

        switch (dialogAction) {
            case DIALOG_OK_ACTION_NONE:
            case DIALOG_OK_ACTION_DONE:
                showOk = 8;
            case DIALOG_OK_ACTION_ON_BACK_PRESS:
            case DIALOG_OK_ACTION_START_PLAY_STORE_APP:
                showOk = 1;
                break;
            case DIALOG_OK_ACTION_TICKETS_CALL:
                showOk = 2;
                break;
            case DIALOG_OK_ACTION_TOP_BALANCE:
                showOk = 3;
                break;
            default:
                break;
        }
        return showOk;
    }



    private void onDialogButtonClick(int ok) {
        switch (dialogAction) {

            case DIALOG_OK_ACTION_ON_BACK_PRESS:
                mListener.onDialogOkClick(MandeAlertDialog.this);

            case DIALOG_OK_ACTION_DONE:
                getDialog().dismiss();
                mListener.onDialogOkClick(MandeAlertDialog.this);
            case DIALOG_CANCEL_ACTION_ON_BACK_PRESS:
                getDialog().dismiss();
                mListener.onDialogOkClick(MandeAlertDialog.this);
                break;
            case DIALOG_OK_ACTION_START_PLAY_STORE_APP:
                if (ok == 1) {
                    mListener.onDialogOkClick(this);
                }
                getDialog().dismiss();
                break;
            case DIALOG_OK_ACTION_TICKETS_CALL:
                if (ok == 1)
                    mListener.onDialogOkClick(this);
                if (ok == 2)
                    mListener.onDialogOk2Click(this);
                getDialog().dismiss();
                break;
            case DIALOG_OK_ACTION_TOP_BALANCE:
                if (ok == 1)
                    mListener.onDialogOkClick(this);
                if (ok == 2)
                    mListener.onDialogOk2Click(this);
                getDialog().dismiss();
                break;
            default:
                getDialog().dismiss();
                break;
        }
    }

    public void showDialog(android.support.v4.app.FragmentManager fm, String msg, int dialogAction) {
        this.dialogAction = dialogAction;
        show(fm, "");
    }

    public void setOkText(String text) {
        btnOk_close.setText(text);
    }

    public void setCancelText(String text) {
        btn_ticket.setText(text);
    }



}
