package com.mande.dialog;

 

 
import com.example.mande.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AppOpenDialog extends Dialog {

	Context context;
	String title, message;
	TextView mTitle, mMessage;
	Button mExit;
	Button mOk;

	public AppOpenDialog(Context context, String title, String message) {
		super(context);
		this.context = context;
		this.title = title;
		this.message = message;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_open);
		init();
	}
	
	private void init() {
		//Typeface typeface = Typefaces.get(getContext(),
			//	"fonts/myriad-web-pro.ttf");

		mTitle = (TextView) this.findViewById(R.id.dialog_header);
		mTitle.setText(title);
		//mTitle.setTypeface(typeface);

		mMessage = (TextView) this.findViewById(R.id.dialog_message);
		mMessage.setText(message);
		//mMessage.setTypeface(typeface);

		mExit = (Button) this.findViewById(R.id.button1);
		mExit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppOpenDialog.this.dismiss();
			}
		});

		mOk = (Button) this.findViewById(R.id.buttonCloseDialog);
	//	mOk.setTypeface(typeface);
		mOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AppOpenDialog.this.dismiss();
				
			}
		});
		
		((Button) this.findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View arg0) {
				AppOpenDialog.this.dismiss();
			}
		});
	}
	 
	/*
	 * @param listener You have to call the show method before you can customize
	 * the OK button click listener
	 */
	public void setOkClickedAction(String text,View.OnClickListener listener) {
		mOk.setText(text); 
		mOk.setOnClickListener(listener);
	}
	
	public void setOkClickedAction(View.OnClickListener listener) {
		mExit.setOnClickListener(listener);
	}
	
	public void setyesClickedAction(View.OnClickListener listener) {
		mOk.setOnClickListener(listener);
	}
	
	public interface Test extends Runnable{
		
	}
}

