package com.mande.adapter;
  
import com.example.mande.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {

	LayoutInflater inflater;
	private Integer[] images;
	private String[] titles;
	private int color= Color.BLACK;

	public ImageAdapter(Context c, Integer[] images,String[] titles) {
		inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.images = images;
		this.titles = titles;
	}
	
	public ImageAdapter(Context c, Integer[] images,String[] titles, int color) {
		inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.images = images;
		this.titles = titles;
		this.color = color;
	}
	

	public int getCount() {
		return images.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" }) public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		TextView label;

		convertView = inflater.inflate(R.layout.menu_items, null);
		imageView = (ImageView) convertView.findViewById(R.id.imgMenuImageView);
		label = (TextView) convertView.findViewById(R.id.txtTitle);

		convertView.setLayoutParams(new GridView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		imageView.setImageResource(images[position]);
		//Typeface typeface = Typefaces.get(convertView.getContext(), "fonts/myriad-web-pro.ttf");
		//label.setTypeface(typeface);
		label.setTextColor(color);
		label.setTextAppearance(convertView.getContext(), Typeface.BOLD);
		label.setText(titles[position]);

		return convertView;
	}
}