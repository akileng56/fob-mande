package com.mande.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.mande.EditStory;
import com.example.mande.MyProjects;
import com.example.mande.MyStories;
import com.example.mande.NewStory;
import com.example.mande.NewStory_withTemplate;
import com.example.mande.R;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class Stories extends ActionBarActivity implements OnClickListener{
	JSONArray cast;String s="", p = "";
	String storyname, Title;
 
	public static final String SELECTED_STORY = "MyPrefs" ;
 
	static final int READ_BLOCK_SIZE = 100;
	ImageView Addstory;
	//String [] values;
	String url; 

	 ProgressDialog pDialog, nDialog;

	 ListView listView;
	 ArrayList<String> data = new ArrayList<String>();
	List<String> allNames = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	 
		
		setContentView(R.layout.activity_stories);	
		
		 
		 listView = (ListView) findViewById(R.id.listView2);
		 
		
	        new GetData().execute();
	        
		  
	}
	
 
	@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	for (int i=0; i<menu.size(); i++) {
    		MenuItem mi = menu.getItem(i);
    		String title = mi.getTitle().toString();
    		Spannable newTitle = new SpannableString(title);        
    		newTitle.setSpan(new ForegroundColorSpan(Color.BLUE), 0, newTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    		mi.setTitle(newTitle);
    	}
    	return true;
	}

	
 
	private class GetData extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Stories.this);
 
			pDialog.setMessage("Retreiving Stories....");
 
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"RetrieveStories?" );
			
 
 
			 
			 SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);		      
 
			 String h = settings.getString("Project", null); 
		
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("ProjectName",h));				

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {		 

			if (result != null) 
				
			{
                try
                {
                    data.clear();
                    JSONArray array = new JSONArray(result);

                    for(int a=0;a<array.length();a++){
                    	    
 
   	                    	 JSONObject p = array.getJSONObject(a);  	                      
 

   	                    	Title = p.getString("Title");
  	                            data.add(Title);  	                        
  	                            
  	                            
                    }
                    
                    if(data.size() > 0)
                    {
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Stories.this, 
                                android.R.layout.simple_list_item_1, data);

                        listView.setAdapter(adapter);
                        
                        listView.setOnItemClickListener(new OnItemClickListener() {
                            

							@SuppressLint("NewApi")
							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, final int position, long id) {
								// TODO Auto-generated method stub
								storyname = (String) parent.getItemAtPosition(position);
 
								
								SharedPreferences settings = getSharedPreferences(SELECTED_STORY ,0);
							    SharedPreferences.Editor editor = settings.edit();
							    editor.putString("Story",storyname);
							    editor.commit();					
								
 
								AlertDialog.Builder builder = new AlertDialog.Builder(Stories.this);
								
								builder.setTitle(storyname);
								
 
								builder.setItems(new CharSequence[] { "STORY DETAILS","EDIT STORY"},
 
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int which) {
												switch (which) {
												case 0:
													 //story details go here													
												     Intent gotoStoryDetails = new Intent(Stories.this,StoryDetails.class);
												     gotoStoryDetails.putExtra("name", storyname);													     
												     startActivity(gotoStoryDetails);
												    
													break;
												case 1:
													//edit details go here
													  Intent gotoEditStory = new Intent(Stories.this,EditStory.class);
													  gotoEditStory.putExtra("name", storyname);													     
													     startActivity(gotoEditStory);
													
													break;
																									 
												}
											}
										});
								builder.create().show();
							}
                        });
                        
                        
                        //put for Adding story
               
                		Addstory = ((ImageView) findViewById(R.id.StoriesMenu));
                		
                		Addstory.setOnClickListener(new View.OnClickListener() {
                		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                			@SuppressLint("NewApi")
                			@Override
                		    public void onClick(View v) {
                		        PopupMenu popup = new PopupMenu(Stories.this, Addstory);
 
                		        
 
                		        //Inflating the Popup using xml file
                		        popup.getMenuInflater()
                		                .inflate(R.menu.stories, popup.getMenu());

                		        //registering popup with OnMenuItemClickListener                		        
                		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                		            public boolean onMenuItemClick(MenuItem item) {

                		                switch (item.getItemId()){
                		                
                		                    case R.id.AddStory:                		                    	   
                		                    	new CheckStory().execute();	
                		                        break;
                		                        
                		                    case R.id.MyStories:                		                   	                    
                		                    	startActivity(new Intent(Stories.this,MyStories.class)); 
                		                        break;
                		                        
                		                    case R.id.logout:
                		                    	startActivity(new Intent(Stories.this,Login.class));                 		                   
                		                        break;

                		                }

                		                return true;
                		            }
                		        });

                		        popup.show();
                		    }
                		});
                	    
  
                        
                    }
                    else
                    {
                        Toast.makeText(Stories.this, "No Stories Found", Toast.LENGTH_LONG).show();
                    }

                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
				else {				
				
				//Methods.showDialog(getSupportFragmentManager(), msg, title, ItecAlertDialog.DIALOG_OK_ACTION_NONE);
			} 				
			pDialog.dismiss();
		} 
	
}
	
	  
	  
		private class CheckStory extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
			protected void onPreExecute() {
				super.onPreExecute();
				pDialog = new ProgressDialog(Stories.this);
				pDialog.setMessage("Loading...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(true);
				pDialog.show();
			}
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				
				HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
						+"NewStory?" );
				 
				SharedPreferences settings = getSharedPreferences(Login.LOGGED_IN_USER,0);
			      
				 String person = settings.getString("FullName", null); 
			 
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("FullName",person));				

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				Log.e("url check", ""+httpPost);
				new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
				return null;
			} 
			
		
			
			public void onCallBack(String result) {
				// TODO Auto-generated method stub			 
				String resultCode, resultMsg;

				if (result != null) {
				
					JSONObject o;
					try { 
						o = new JSONObject(result);

						if (o.length() > 0) { 
							
							resultCode = o.getString("ResultCode");
							resultMsg = o.getString("ResultMessage");	
							
							 SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);		      
							 String h = settings.getString("Project", null);
							
 
							if (resultCode.equals("200")) {	
								
								SharedPreferences Projectsetting = getSharedPreferences(MyProjects.Project_Has_Template ,0);
								
								 String check_for_template = Projectsetting.getString("HasTemplate", null);
								 
								 //check if there is no template
								   if(check_for_template == "false"){
										Toast.makeText(getApplicationContext(),h, Toast.LENGTH_LONG).show();
									    Intent gotoNewStory = new Intent(Stories.this,NewStory.class);
									    startActivity(gotoNewStory);
								        pDialog.dismiss();
									   
								   }								 
								   else{
									   
									    
										Toast.makeText(getApplicationContext(),"Project has template", Toast.LENGTH_LONG).show();
										Intent gotoNewStory_withTemplate = new Intent(Stories.this,NewStory_withTemplate.class);
									    startActivity(gotoNewStory_withTemplate);
										pDialog.dismiss();
								   }
							
 
											
								return;
							} else if (resultCode.equals("400")) {

								Toast.makeText(getApplicationContext(),resultMsg, Toast.LENGTH_LONG).show();
								
							} 
						      else if (resultCode.equals("401")) {

							Toast.makeText(getApplicationContext(),resultMsg, Toast.LENGTH_LONG).show();
							
						} 
							
						      else if (resultCode.equals("203")) {

									Toast.makeText(getApplicationContext(),resultMsg, Toast.LENGTH_LONG).show();
									
								} 
						} 
					   }catch (JSONException e) {

						e.printStackTrace(); 
					 
					}

				} else {				
					
					 
				} 				
				pDialog.dismiss();
			} 
		
}
		
		
		
		@Override
		public void onBackPressed() {
		 startActivity(new Intent(Stories.this,MainMenuActivity.class));
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.story_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
			//startActivity(new Intent(Stories.this,NewStory.class));
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
	}
}
