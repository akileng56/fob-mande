package com.mande.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.example.mande.R;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class ReportingPeriod extends ActionBarActivity {
  String Projectname;
  ProgressDialog pDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_reporting_period);
		Bundle bundle = getIntent().getExtras();
		Projectname = bundle.getString("Projname");
		
		new LoginCheckAsyncTask().execute();
	}

	
	private class LoginCheckAsyncTask extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportingPeriod.this);
			pDialog.setMessage("Loading......");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"GetReportingPeriod?" );			
	
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Name",Projectname)); 
			 

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {
		

			if (result != null) {
			
				            
                 	    
	                    	 JSONObject o; 
				try { 
					 JSONArray array = new JSONArray(result);
					o =  array.getJSONObject(0);

					if (o.length() > 0) { 
						String closeDate = o.getString("CloeDate");
						String startDate = o.getString("StartDate");						  
						String Status = o.getString("Status");
						String Storyinputd = o.getString("StoryInputD");
						
						
						TextView getTitle = (TextView) findViewById(R.id.textView1);
						getTitle.setText(Projectname);
						
						TextView getDomain = (TextView) findViewById(R.id.textView6);
						getDomain.setText(startDate);
						
						TextView getStoryteller = (TextView) findViewById(R.id.textView7);
						getStoryteller.setText(closeDate);
						
						TextView getCollectedBy = (TextView) findViewById(R.id.textView8);
						getCollectedBy.setText(Status);
						
						TextView getSummary = (TextView) findViewById(R.id.textView9);
						getSummary.setText(Storyinputd );
						
					}
				} catch (JSONException e) {

					e.printStackTrace(); 
			 
				}

			} 
			
			pDialog.dismiss();
		} 
	

	}  
}
