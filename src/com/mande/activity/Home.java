package com.mande.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

import com.example.mande.R;
import com.mande.helper.ExpandableListAdapter;

public class Home extends ActionBarActivity {
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    ExpandableListView Exlist;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.expandableListView1);
 
        // preparing list data
        prepareListData();
 
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
 
        // setting list adapter
        expListView.setAdapter(listAdapter);
        expListView.expandGroup(0);
        expListView.expandGroup(1);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {          

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				 // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
				// TODO Auto-generated method stub
				return false;
			}
        });
 
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
 
            @Override
            public void onGroupExpand(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
 
        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
 
            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/
 
            }
        });
 
        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
         String childData = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                // TODO Auto-generated method stub
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                        listDataHeader.get(groupPosition)).get(
                                        childPosition), Toast.LENGTH_SHORT)
                        .show();*/
            	  if(childData.equalsIgnoreCase("System Setup")){        
      				 // startActivity(new Intent(Home.this,SystemSetup.class));
            	  }
            	  else if(childData.equalsIgnoreCase("Organisations")){
            		  //startActivity(new Intent(Home.this,Organisations.class));
            	  }
            	  else if(childData.equalsIgnoreCase("Accounts")){
            		  //startActivity(new Intent(Home.this,Accounts.class));
            	  }
                return false;
            }
        });
	}

	  private void prepareListData() {
	        listDataHeader = new ArrayList<String>();
	        listDataChild = new HashMap<String, List<String>>();
	 
	        // Adding child data
	        listDataHeader.add("Administration");
	        listDataHeader.add("System Logs");
	        
	 
	        // Adding child data
	        List<String> Administration = new ArrayList<String>();
	        Administration.add("System Setup");
	        Administration.add("Organisations");
	        Administration.add("Accounts");
	        Administration.add("Export Data");	        
	        
	        List<String> SystemLogs = new ArrayList<String>();
	        SystemLogs.add("Active Sessions");
	        SystemLogs.add("Scheduled Events");
	        SystemLogs.add("RunTime Instances");
	        SystemLogs.add("RunTime Statistics");
	        SystemLogs.add("Mail Logs");
	      
	 

	        listDataChild.put(listDataHeader.get(0), Administration); // Header, Child data
	        listDataChild.put(listDataHeader.get(1), SystemLogs);
	        //listDataChild.put(listDataHeader.get(2), comingSoon);
	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
