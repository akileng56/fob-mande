package com.mande.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mande.EditStory;
import com.example.mande.R;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class StoryDetails extends Activity implements OnClickListener {
	String Storyname;
	String url;
	String Title, Domain, Storyteller, CollectedBy, Summary, ReportingPeriod;
	ImageView EditStory;
	ProgressDialog pDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		
		setContentView(R.layout.story_details);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		Bundle bundle = getIntent().getExtras();
		Storyname = bundle.getString("name");
		
		new LoginCheckAsyncTask().execute();

		//Toast.makeText(getApplicationContext(),name, Toast.LENGTH_LONG).show();
		//new GetData().execute();
		
		EditStory = (ImageView)findViewById(R.id.Add);
		EditStory.setOnClickListener(this);
		
	}


	
	private class LoginCheckAsyncTask extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(StoryDetails.this);
			pDialog.setMessage("Loading......");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"StoryDetails?" );			
	
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Title",Storyname)); 
			 

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {
		 
			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						Title = o.getString("Title");
						Domain = o.getString("Domain");
						Storyteller = o.getString("StoryTeller");
						CollectedBy = o.getString("CollectedBy");
						Summary = o.getString("Summary");
						ReportingPeriod = o.getString("ReportingPeriod");
						
						TextView getTitle = (TextView) findViewById(R.id.textView2);
						getTitle.setText(Title);
						
						TextView getDomain = (TextView) findViewById(R.id.textView8);
						getDomain.setText(Domain);
						
						TextView getStoryteller = (TextView) findViewById(R.id.textView9);
						getStoryteller.setText(Storyteller);
						
						TextView getCollectedBy = (TextView) findViewById(R.id.textView10);
						getCollectedBy.setText(CollectedBy);
						
						TextView getSummary = (TextView) findViewById(R.id.textView11);
						getSummary.setText(Summary);
						
						TextView getReportingPeriod = (TextView) findViewById(R.id.textView12);
						 getReportingPeriod.setText(ReportingPeriod);
					} 
				} catch (JSONException e) {

					e.printStackTrace(); 
					//Toast.makeText(context, title+": "+msg, Toast.LENGTH_LONG).show();
					
					//Methods.showDialog(getSupportFragmentManager(), msg, title, ItecAlertDialog.DIALOG_OK_ACTION_NONE);
				}

			} else {				
				
				//Methods.showDialog(getSupportFragmentManager(), msg, title, ItecAlertDialog.DIALOG_OK_ACTION_NONE);
			} 
			
			pDialog.dismiss();
		} 
	

	}  

	 	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.story_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.Add) {
			Intent gotoEditStory = new Intent(this,EditStory.class);
			  gotoEditStory.putExtra("name", Storyname);													     
			     startActivity(gotoEditStory);
		}
		
	}
}
