package com.mande.activity;

 

   
 
import com.example.mande.MyProjects;
import com.example.mande.R;  
import com.mande.adapter.ImageAdapter;
import com.mande.dialog.AppOpenDialog;
import com.mande.utils.MenuValues;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity; 
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener; 
import android.widget.GridView; 
import android.widget.ImageView;
import android.widget.PopupMenu;  
 

public class MainMenuActivity extends Activity implements OnClickListener, OnItemClickListener{

	public static Typeface typeface; 
	private ImageView menubtn; 
	private GridView gridview;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		 
		setContentView(R.layout.activity_menu); 
	
		/*getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/
	 
		
		//typeface = Typefaces.get(this,"fonts/OSP-DIN.ttf"); 

		
		initData();
		
		
	}
	 
	private void initData() { 
		 

		gridview = (GridView) findViewById(R.id.gvMenuItems);
		gridview.setAdapter(new ImageAdapter(this, MenuValues.menu_imgItems,
				MenuValues.student_titles));
		gridview.setOnItemClickListener(this);
		gridview.setBackgroundColor(Color.TRANSPARENT); 
		 
		menubtn = ((ImageView) findViewById(R.id.exit1));
		
		menubtn.setOnClickListener(new View.OnClickListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@SuppressLint("NewApi")
			@Override
		    public void onClick(View v) {
		        PopupMenu popup = new PopupMenu(MainMenuActivity.this, menubtn);
		        //Inflating the Popup using xml file
		        popup.getMenuInflater()
		                .inflate(R.menu.popup_menu, popup.getMenu());

		        //registering popup with OnMenuItemClickListener
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {

		                switch (item.getItemId()){
		                    case R.id.help:
		                        /*getSupportFragmentManager().beginTransaction()
		                                .add(R.id.container, new SelfCareCategorySelectionFragment()).addToBackStack(null)
		                                .commit();*/
		                        break;
		                    case R.id.logout:
		                        Intent intent = new Intent(MainMenuActivity.this, Login.class);
		                        startActivity(intent);
		                        finish();
		                        break;
		                    case R.id.settings:
		                        Intent i = new Intent(MainMenuActivity.this, AppSettings.class);
		                        startActivity(i);
		                        break;

		                }

		                return true;
		            }
		        });

		        popup.show();
		    }
		});
 
		
	}
	
 
	@Override
	public void onClick(View v) {	
		
		
		
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position,
			long id) { 
		
      switch(position){
		
		case 0:			
			startActivity(new Intent(MainMenuActivity.this,Stories.class));			
			break;
	
		
		case 1:	
			startActivity(new Intent(MainMenuActivity.this,Tasks.class));
			break; 
			
			
		case 2:				
			startActivity(new Intent(MainMenuActivity.this,ActiveProjects.class));
			break; 
	default:
		break;
		}
		
      
	}
 
	public void onBackPressed() {
	 startActivity(new Intent(MainMenuActivity.this,MyProjects.class));
	}
 


	}
 



 
