package com.mande.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.mande.R;

public class Splash extends ActionBarActivity {
	TextView textViewFOBUrl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	
		setContentView(R.layout.activity_splash);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//textViewFOBUrl = (TextView) findViewById(R.id.FOBUrl);
		//textViewFOBUrl.setTypeface(FontManager.getFonts(getApplicationContext(), "Dosis-Medium.ttf"));
		
		/**
		 * Take 3 seconds before showing login
		 */
		new Handler().postDelayed(new Runnable() {			
			@Override
			public void run() {

				Intent i = new Intent(Splash.this,Login.class);

				startActivity(i);						
				// close this activity
				finish();	
			}
		}, 3000);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
