package com.mande.activity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Bundle;

import com.example.mande.R;

public class AppVersionNo extends Activity{
	
	private EditText txtCallbackUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) { 

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		
        setContentView(R.layout.fragment_app_version_no);

        TextView title = (TextView) findViewById(R.id.lblSettings);
       // title.setTypeface(Utils.setBoldFont(this));

        txtCallbackUsername = (EditText) findViewById(R.id.txtCloudbancUsername);
        //txtCallbackUsername.setText(Utils.KEY_APP_VERSION_NO);
        try {
            String versionName = getPackageManager().getPackageInfo(
                    getPackageName(), 0).versionName;
            txtCallbackUsername.setText(versionName);
            txtCallbackUsername.setTextColor(Color.RED);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


}
