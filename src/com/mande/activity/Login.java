package com.mande.activity;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mande.helper.Methods;
import com.example.mande.MyProjects;
import com.example.mande.R;
import com.mande.dialog.MandeAlertDialog;
import com.mande.helper.SPManager;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;
import com.mande.utils.Method;


public class Login extends ActionBarActivity implements OnClickListener, MandeAlertDialog.MandeDialogListener {
	Button login;	
	 EditText Username ,Password;
	 ProgressDialog pDialog;
	 
	 public static final String LOGGED_IN_USER = "MyPrefs" ;
	 
	 
	//String url = "http://192.168.1.15:8089/rest/ws_getuser/";
	//private static final String TAG_SUCCESS = "success";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		
		setContentView(R.layout.activity_login);
		 
		//sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
	
		
		Username = (EditText) findViewById(R.id.editTextEmail);
		Password =(EditText)findViewById(R.id.editTextPassword);
		
		 		
		login = (Button)findViewById(R.id.buttonLogin); 
		login.setOnClickListener(new View.OnClickListener() 
        {		
			@Override
			public void onClick(View view) {
				String pass = Password.getText().toString();
				String emal = Username.getText().toString();
				
				if (emal != null && emal.length() > 0) {
					
					if (pass != null && pass.length() > 0) { 
					
					
						new LoginCheckAsyncTask().execute(); 

					} else // for password validation
	            	{
	            		Method.showDialog(
	                        getSupportFragmentManager(),
	                        getResources().getString(
	                                R.string.txt_msg_invalid_passw),
	                        getResources().getString(
	                                R.string.txt_title_invalid_passw),
	                        MandeAlertDialog.DIALOG_CANCEL_ACTION_NONE);
	            	}
				}else // for email validation
				{
					
					Method.showDialog(
	                        getSupportFragmentManager(),
	                        getResources().getString(
	                                R.string.txt_msg_invalid_email),
	                        getResources().getString(
	                                R.string.txt_title_invalid_email),
	                        MandeAlertDialog.DIALOG_CANCEL_ACTION_NONE);	
					
				}
			}
		});
	}
	
	String Username_login,Password_login;
	

	private class LoginCheckAsyncTask extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Login.this);
			pDialog.setMessage("Siging in...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"login?" );
			
			
			//Getting What the User has Entered
			Username_login = Username.getText().toString();
			Password_login = Password.getText().toString();
			
		  
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Username",Username_login)); 
			nameValuePairs.add(new BasicNameValuePair("Password",Password_login)); 

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
		
		
		public void onCallBack(String result) {
			// TODO Auto-generated method stub
			String msg = "Server Unreachable.";
			String title = "Connection Failure";
			String resultCode, resultMsg, userRole, FullName;

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						
						resultCode = o.getString("ResultCode");
						resultMsg = o.getString("ResultMessage");
						userRole = o.getString("UserRole");
						FullName = o.getString("FullName");
						
						
						Log.e("FullName: ", FullName);
						Log.i("UserRole :", userRole);
						 try {
							 FileOutputStream fileout=openFileOutput("UserNamefile.txt", MODE_PRIVATE);
							 OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
							 outputWriter.write(FullName);
							 outputWriter.close();
							 
							 //display file saved message
							 Toast.makeText(getBaseContext(), FullName+" Saved successfully!",
							 Toast.LENGTH_SHORT).show();
							 
							 } catch (Exception e) {
							 e.printStackTrace();
							 }
						
						 	SharedPreferences settings = getSharedPreferences(LOGGED_IN_USER ,0);
						    SharedPreferences.Editor editor = settings.edit();
						    editor.putString("FullName",FullName);
						    editor.commit(); 

						if (resultCode.equals("200")) {		
							
						
							switch(userRole){
 
						case "Participant":	
							
 
								startActivity(new Intent(Login.this,MyProjects.class));
							    Username.getText().clear();
								Password.getText().clear(); 
								pDialog.dismiss();
							break;
							
						case "OrganizationAdministrator":
								//startActivity(new Intent(Login.this,Home2.class));
							    Username.getText().clear();
								Password.getText().clear(); 
								pDialog.dismiss();	
								
								
							break;
							case "ProjectAdministrator":								
								startActivity(new Intent(Login.this,Home.class));
							    Username.getText().clear();
								Password.getText().clear(); 
								pDialog.dismiss();
							
								break;
								
							case "default":
							 
								break;	
							}	
							
							return;
						} else if (resultCode.equals("201")) {
							
							Method.showDialog(getSupportFragmentManager(),
									resultMsg, title,
	                                MandeAlertDialog.DIALOG_CANCEL_ACTION_NONE);
							
						} 
					} 
				} catch (JSONException e) {

					e.printStackTrace(); 
	                if(pDialog.isShowing()){
	                	pDialog.dismiss();
	                }
	                //title = "Error";
	               // msg = "Please check your internet connection and try again. thank you";
	                Method.showDialog(getSupportFragmentManager(),
	                        msg, title,
	                        MandeAlertDialog.DIALOG_CANCEL_ACTION_NONE);
	                return;
				}

			} else { 
				if(pDialog.isShowing()){
					pDialog.dismiss();
	            }
	           // title = "Error";
	          //  msg = "Please check your internet connection and try again. thank you";
	            Method.showDialog(getSupportFragmentManager(),
	                    msg, title,
	                    MandeAlertDialog.DIALOG_CANCEL_ACTION_NONE);
	            return;
			} 
			
			pDialog.dismiss();
		} 
	

	}  	 

	

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDialogOkClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDialogDismiss(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDialogOk2Click(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
