
package com.mande.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mande.MyProjects;
import com.example.mande.R;
import com.mande.helper.ServiceHitListner;
import com.mande.helper.WsExecuter;

public class ActiveProjects extends ActionBarActivity {
	 ListView listView;
	 Spinner spindrop;
	 Button ReportingP, SelectionPanel;
	 String Projectname,h;
	 ArrayList<String> data = new ArrayList<String>();
	List<String> allNames = new ArrayList<String>();
	String s="";ProgressDialog pDialog;
	String storyname;
	static final int READ_BLOCK_SIZE = 100;
	ImageView Addstory;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_active_projects);
		 
		new GetActiveProjectDetails().execute();
		
	}

	
  
	
	
	private class GetActiveProjectDetails extends AsyncTask<Void, Void, Void> implements ServiceHitListner { 
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ActiveProjects.this);
			pDialog.setMessage("Loading......");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			HttpPost httpPost = new HttpPost(WsExecuter.BASE_URL
					+"ActiveProjectDetails?" );	
			
			SharedPreferences settings = getSharedPreferences(MyProjects.SELECTED_PROJECT,0);                		      			      
			    h = settings.getString("Project", null); 
	
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("Name",h)); 
			 

			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Log.e("url check", ""+httpPost);
			new WsExecuter(httpPost, null, WsExecuter.TYPE_POST, this);
			return null;
		} 
		
	
		
		public void onCallBack(String result) {			

			if (result != null) {
			
				JSONObject o;
				try { 
					o = new JSONObject(result);

					if (o.length() > 0) { 
						String StoriesCollected = o.getString("StoriesCollected");
						String MLiked = o.getString("MostLiked");
						String MCommented = o.getString("MostCommented");
						String MRated = o.getString("MostRated");
						
						
						TextView getCollectd = (TextView) findViewById(R.id.textView3);
						getCollectd.setText(StoriesCollected);
						
						TextView getMliked = (TextView) findViewById(R.id.textView6);
						getMliked.setText(MLiked);
						
						TextView getmcommentd = (TextView) findViewById(R.id.textView8);
						getmcommentd.setText(MCommented);
						
						TextView getMrated = (TextView) findViewById(R.id.textView7);
						getMrated.setText(MRated);
						 
						
					} 
				} catch (JSONException e) {

					e.printStackTrace(); 
		 
				}

			}  
			
			pDialog.dismiss();
			
			ReportingP = (Button)findViewById(R.id.button1);
			ReportingP.setOnClickListener(new View.OnClickListener() 
	        {		
				@Override
				public void onClick(View view) {				
					Intent gotoReporting = new Intent(ActiveProjects.this,ReportingPeriod.class);
					gotoReporting.putExtra("Projname",h);
					startActivity(gotoReporting);
				}
			});
		 
	 
		} 
	

	}  
	
	

}

