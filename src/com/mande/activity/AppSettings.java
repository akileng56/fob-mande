package com.mande.activity;

import com.example.mande.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class AppSettings extends Activity implements OnClickListener{
	

    TextView btnAppVersionNod, btnStyleElements,  btnCloudbanc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
         
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		
        setContentView(R.layout.fragment_app_settings_main);
        initData();
 
    }

    
    private void initData() {

        btnAppVersionNod = (TextView) findViewById(R.id.btnConnectMeSettings);

        btnStyleElements = (TextView) findViewById(R.id.btncloudbanc);


        btnAppVersionNod.setOnClickListener(this);

        btnStyleElements.setOnClickListener(this);
    }
     


	@Override
	public void onClick(View v) {


        switch (v.getId()) {

            case R.id.btnConnectMeSettings:
                Intent mainIntent = new Intent(this, AppVersionNo.class);
                startActivity(mainIntent);
                break;

            case R.id.btncloudbanc:
                Intent mainIntent1 = new Intent(this, StyleElementsActivity.class);
                startActivity(mainIntent1);
                break;


            default:
                break;
        }


    
		
	}
}
